package aws.main.service;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import net.spy.memcached.MemcachedClient;

import org.json.JSONArray;

import aws.main.model.QueueModelObject;
import aws.main.utility.Utility;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQSClient;

/**
 * 
 * @author pawan This project is used for uploading users in queue from database
 *         so that we can avoid database dependency
 *
 *         This is the main class for this project
 *
 *
 *
 */

public class MainClass {

	/**
	 * 
	 * All important variable used in this class
	 */
	public static String queueCountString = "";
	public static boolean isFromDashBoard = true;
	// now all process are combined so keep this flag true
	// public static boolean isCombineProcessess = true;
	public static String queueArray[];
	public static String queueName_forWhichIsMade;
	public static boolean isRampupcompleted = false;

	/**
	 * This is a static block of this class
	 * 
	 * it will basically used for creating connection and memcache object
	 * 
	 */
	static {
		if (Utility.memcacheObj == null) {
			try {

				// initializing memecache object
				Utility.memcacheObj = new MemcachedClient(new InetSocketAddress("127.0.0.1", 11211));

				try {
					Utility.credentials = new BasicAWSCredentials(Utility.awsAccessKey, Utility.awsSecretKey);
					Utility.sqs = new AmazonSQSClient(Utility.credentials);
					Utility.sqs.setRegion(Utility.queueRegion);
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Utility.initializeQueueHashMap();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 
	 * 
	 * @param ar
	 * 
	 *            This is main thread of execution which will start the
	 *            execution of this project
	 */

	public static void main(String ar[]) {

		try {
			try {
				queueArray = ar;
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			System.out.println(Utility.HOST_NAME);
			// for one particaular queeue code making
			try {
				if (ar != null && ar.length > 0) {
					for (int i = 0; i < ar.length; i++) {
						System.out.println(ar.length + "" + ar[i]);
						queueName_forWhichIsMade = queueName_forWhichIsMade + ar[i] + ",";
					}

				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("value=" + MainClass.queueName_forWhichIsMade);
			Utility.gmtScheduleSendDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			Utility.sendgrid_currdate = new SimpleDateFormat("dd-M-yyyy").format(new Date());
			Utility.lasthour_morning = Utility.startHour_morning;
			Utility.lasthour_evening = Utility.startHour_evening;

			// =====================================//===========================================================

			try {

				String jsonData = null;
				try {
					if (isFromDashBoard)
						jsonData = Utility.getContent("http://s2.oakjobalerts.com:8080/UserProcessLifecycle/queueData");
					else
						jsonData = Utility.getContent("http://s2.oakjobalerts.com:8080/UserProcessLifecycle/queueData");

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (jsonData != null && jsonData.length() > 0) {

					try {
						Utility.genrateQueuemodelList(new JSONArray(jsonData));
					} catch (Exception e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();
					}

					// initialize oakj search keyword hash map and white label
					// as
					// weell
					try {
						Utility.initializeMemcacheFor_Oak_TheKeyword();
						Utility.initializeChannelWithProvider();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					try {
						Utility.initializesearch_keyword_for_oakjob_newApi();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// for 421_users

					// getting all invalid domain

					try {
						Utility.readRampupFromDb();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//

					try {
						Utility.iniTialize_map_for_421_users();
						// here we are performing the rampup of new code

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						Utility.getInvalidDomain();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					try {

						Utility.initialize_domain_campaign_wise_map(Utility.filePath + "/mis_logs/Domain_Compaign/domain_compaign.json");
					} catch (Exception e2) {
						// TODO Auto-generated catch block

					}
					try {
						String path = Utility.filePath + "/mis_logs/DomainWiseUsers";

						path = path + File.separator + "domain_wise_user_" + Utility.dateFormat.format(new Date()) + ".json";
						System.out.println(path);
						Utility.initialize_domain_wise_statst_map(path);

					} catch (Exception e2) {
						// TODO Auto-generated catch block

					}
					try {
						String path = Utility.filePath + "/mis_logs/ProcessWiseUserStats";

						path = path + File.separator + "process_wise_user_" + Utility.dateFormat.format(new Date()) + ".json";
						System.out.println(path);
						Utility.initialize_process_wise_statst_map(path);

					} catch (Exception e2) {
						// TODO Auto-generated catch block

					}
					try {
						String path = Utility.filePath + "/mis_logs/InvalidDomainWiseUsers";

						path = path + File.separator + "invalid_domain_wise_user_" + Utility.dateFormat.format(new Date()) + ".json";
						System.out.println(path);
						Utility.initialize_invalid_domain_wise_statst_map(path);

					} catch (Exception e2) {
						// TODO Auto-generated catch block

					}

				}

				if (Utility.inValidDomainList.size() != 0) {

					Utility.invalidDomainString = Utility.invalidDomainString();

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// =======================================

			// wxecute only when one queue is made from the mobile app
			if (queueName_forWhichIsMade == null) {
				if (Utility.numberOfActiveQueueCount > 25 && isFromDashBoard) {

					// here we are performing the rampup of new code
					for (int i = 0; i < Utility.rampUpModelist.size(); i++) {

						Utility.perFormRampupAndUpdateInDataBase(Utility.rampUpModelist.get(i));

					}

					// here we are checking that number of process which remains
					// pending for rampup
					Utility.rampUpModelist.clear();
					Utility.readRampupFromDb();

					if (Utility.rampUpModelist.size() != 0) {

						for (int i = 0; i < Utility.rampUpModelist.size(); i++) {

							Utility.sendSms("Rampup of " + Utility.rampUpModelist.get(i).getProcessName() + " advertisement=" + Utility.rampUpModelist.get(i).getRampup_advertisement() + " has not performed today.Please check this on priority!!");
							Utility.sendEmailJustForAlerts("Rampup of " + Utility.rampUpModelist.get(i).getProcessName() + " advertisement=" + Utility.rampUpModelist.get(i).getRampup_advertisement() + " has not performed today.Please check this on priority!!");
						}
					}

				}
			}

			for (int i = 0; i < Utility.queuesArrayList.size(); i++) {

				Utility.totalUsersInDb = 0;
				// iniitialise the following vaiabl
				int offset = 1000;
				int fromIndex = 0;
				int uptoWhichRecord = 0;
				int interval = 0;
				double totalRecordInDb = 0;

				// here we make the object of model class
				QueueModelObject model = Utility.queuesArrayList.get(i);

//				 we have stop sending fresher,weekly monthly users on 4th july
				// 17
				if (model.getIsActive().equalsIgnoreCase("1") && model.getSelecteddays().contains(Utility.getTheCurrentDayWeek())
				
				) {

				
					String pixelcateGoryArray[] = model.getPixelCategoryName().split("\\|");
					String sendGridCategory[] = model.getSendGridCategory().split("\\|");
					// process user having no keyword

					model.setQuery(model.getQuery().replace("(LENGTH(KEYWORD)>2) AND ", ""));

					
					model = Utility.getDbData(model.getQuery(), model);
					
					totalRecordInDb = model.totalRecordInDb;

					model.setQuery(model.getQuery().replaceAll("DISTINCT count\\(\\*\\) AS rowcount", "\\*"));

					String queuesData = model.getQueueName();
					String queueNamesArray[] = queuesData.split("\\|");

					if (totalRecordInDb != 0) {
						interval = (int) (totalRecordInDb / queueNamesArray.length);

						for (int j = 0; j < queueNamesArray.length; j++) {

							// ========================================code to
							// create the campaign ids dynamically over the
							// mailgun===================================
							if (!model.getCampaign_id().equalsIgnoreCase("")) {
								String domain_comaing_status_key = model.getDomain_name() + "_" + model.getProcessType() + "_" + model.getCampaign_id();
								if (Utility.domain_campaign_status_map.get(domain_comaing_status_key) == null) {
									String response = Utility.CreateCampaign(model);
									if (response.toString().contains("400"))
										System.out.println("Campaing all ready exist");
									Utility.domain_campaign_status_map.put(domain_comaing_status_key, "done");
									Utility.write_domain_campaign_wise_status();

								}
							}
							// ==================================================//======================

							Utility.userProcessingCount.set(0);
							Utility.usersNotProcessed.set(0);
							Utility.duplicateUsers = 0;
							Utility.invalidUsers = 0;
							Utility.pipeContainingKeywordsUsers = 0;
							Utility.nurseWiseKeywordsUsersCount = 0;
							Utility.users_with_421_error = 0;
							Utility.uk_elite_users = 0;
							Utility.numberOfThread = 1;
							Utility.threadCompled = 0;
							Utility.invalid_domain_block_users = 0;
							Utility.searchKeyword_found = 0;
							int weeklyInterval = 0;

							fromIndex = interval * j;
							// checking for the upto from the back end
							if (model.getUpTo().equalsIgnoreCase("0")) {
								uptoWhichRecord = interval * (j + 1);
							} else {
								// initialize upto to the backend values
								if (model.isRampup() && Utility.isTodayDate(model.getToday_rampup_status().toString())) {
									Utility.updateRampupObjectInTable(model);
								}
								uptoWhichRecord = Integer.parseInt(model.getUpTo());

								if (uptoWhichRecord > interval) {
									uptoWhichRecord = interval;
								}
							}

							// setting from index in the code
							if (model.getFrom().equalsIgnoreCase("0")) {
								fromIndex = fromIndex;
							} else {
								if (j == 0) {

									fromIndex = Integer.parseInt(model.getFrom());

								} else {
									fromIndex = fromIndex;

								}
							}
							// chec for the weekly and monthly and skipping
							// jobvitals
							// and for oakjob old opener and clickers
							if ((model.getProcessType().toLowerCase().contains("weekly") || model.getProcessType().toLowerCase().contains("monthly") || model.getProcessType().equalsIgnoreCase("daily-other"))
							// &&
							// (!model.getProcessName().equalsIgnoreCase("Jobvitals")
							// &&
							// !model.getProcessName().equalsIgnoreCase("Cylcon")
							// &&
							// !model.getProcessName().equalsIgnoreCase("Alumni")
							// && !model
							// .getProcessName().equalsIgnoreCase("College-recruiter"))
							) {
								System.out.println("total number of record=" + totalRecordInDb);
								// HERE WE GET THE NUMBER OF DAYS WE GET FROM
								// THE
								// DASHBOARD FOR WEEKLY AND MONTHLY
								int totalNumberOfDaysForWeeklyAndMonthly = model.getSelecteddays().split(" ").length;

								if (Integer.parseInt(Utility.getTheCurrentDayWeek()) == 0) {

									weeklyInterval = (int) (totalRecordInDb / totalNumberOfDaysForWeeklyAndMonthly);
									fromIndex = 6 * weeklyInterval;
									uptoWhichRecord = (int) totalRecordInDb;

								} else {
									weeklyInterval = (int) (totalRecordInDb / totalNumberOfDaysForWeeklyAndMonthly);
									// fromIndex =
									// (Integer.parseInt(Utility.getTheCurrentDayWeek())
									// - 1) * weeklyInterval;
									// uptoWhichRecord = weeklyInterval *
									// Integer.parseInt(Utility.getTheCurrentDayWeek());
									// because now we send from tuesday to
									// thursday
									// if (totalNumberOfDaysForWeeklyAndMonthly
									// == 5) {
									fromIndex = (Integer.parseInt(Utility.getTheCurrentDayWeek()) - 1) * weeklyInterval;
									uptoWhichRecord = weeklyInterval * (Integer.parseInt(Utility.getTheCurrentDayWeek()));

									// }
									if (!model.getUpTo().equalsIgnoreCase("0")) {

										uptoWhichRecord = uptoWhichRecord + Integer.parseInt(model.getUpTo());

									}

								}
								// }

							}

							// ================================================Ramp
							// up code for the same process
							// table===========================================

							// ===============================================//==============================================
							if (uptoWhichRecord > totalRecordInDb) {
								uptoWhichRecord = (int) totalRecordInDb;
								// uptoWhichRecord=(int) totalRecordInDb;
							}

							model.setFrom(String.valueOf(fromIndex));
							model.setUptoWhichUser(uptoWhichRecord);
							// setting queuename in the object
							model.setQueueName(queueNamesArray[j]);
							// setting pixel category name in the model
							model.setPixelCategoryName(pixelcateGoryArray[j]);

							model.setSendGridCategory(sendGridCategory[j]);

							if (model.getUptoWhichUser() < 10000) {
								if (model.getUptoWhichUser() < 1428) {
									offset = model.getUptoWhichUser();
								} else {
									offset = 1000;
								}
							} else {
								if (model.getUptoWhichUser() - Integer.parseInt(model.getFrom()) > 10000)
									offset = 10000;
								else
									offset = 1000;
							}

							// System.out.println("data for this queue=" +
							// model.getFrom() + "   " + uptoWhichRecord);

							try {
								if (Utility.con == null || Utility.con.isClosed()) {

									Utility.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
								}
							} catch (SQLException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Utility.lasthour_morning = Utility.startHour_morning;
							Utility.lastMin_morning = 0;
							Utility.lastSeconds_morning = 0;

							// for evenign alerts

							Utility.lasthour_evening = Utility.startHour_evening;
							Utility.lastMin_evening = 0;
							Utility.lastSeconds_evening = 0;
							// System.out.println("model.getQueueName()" +
							// model.getQueueName());
							if (model.getUptoWhichUser() != 0) {

								DataPopulationThread dataPopulate = new DataPopulationThread(offset, model, Utility.con);
								dataPopulate.insertingUser_DataInQueue(dataPopulate);
								//
								if (Utility.queueCountHashMap_CombineProcess.size() % 10 == 0) {
									try {
										Utility.sendEmail(Utility.finalMailText, Utility.queueCountHashMap_CombineProcess);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							}

						}
						Utility.memcacheObj.flush();
						Utility.writestatsBackToTheFile(Utility.filePath + "/mis_logs/channelnotfound/channelNotFound.txt", Utility.channelNotFound);

					}

				}

			}

			if (Utility.con != null) {
				try {
					Utility.con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			try {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -6);

				String processwise_path = Utility.filePath + "/mis_logs/ProcessWiseUserStats";
				processwise_path = processwise_path + File.separator + "process_wise_user_" + Utility.dateFormat.format(cal.getTime()) + ".json";
				Utility.delete_old_file_from_server(processwise_path);

				String domain_path = Utility.filePath + "/mis_logs/DomainWiseUsers";
				domain_path = domain_path + File.separator + "domain_wise_user_" + Utility.dateFormat.format(cal.getTime()) + ".json";
				Utility.delete_old_file_from_server(domain_path);

				String invalid_domain_path = Utility.filePath + "/mis_logs/InvalidDomainWiseUsers";
				invalid_domain_path = invalid_domain_path + File.separator + "invalid_domain_wise_user_" + Utility.dateFormat.format(cal.getTime()) + ".json";
				Utility.delete_old_file_from_server(invalid_domain_path);

				Utility.sendEmail(Utility.finalMailText, Utility.queueCountHashMap_CombineProcess);
				Utility.sendTextSms(Utility.queueCountHashMap_CombineProcess);

			} catch (Exception e) {
				e.printStackTrace();
			}
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
