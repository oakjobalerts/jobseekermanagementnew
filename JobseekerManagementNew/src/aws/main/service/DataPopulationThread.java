package aws.main.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import aws.main.model.CloudSearchModel;
import aws.main.model.DomainWiseSeekersCountModel;
import aws.main.model.InvalidDomainModel;
import aws.main.model.MapKeyWordModel;
import aws.main.model.ProcessWiseUserModel;
import aws.main.model.QueueModelObject;
import aws.main.utility.Utility;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.google.gson.Gson;
import com.mysql.jdbc.Statement;

/**
 * 
 * @author signity This is our data uploading thread class which is based on
 *         recursion mechanism
 * 
 *         {@link #iniTializeCloudModelObject_ForExtraParameter(QueueModelObject, CloudSearchModel)}
 *         {@link #insertingUser_DataInQueue(DataPopulationThread)}
 */
public class DataPopulationThread {
	// Thread thread;
	Statement st;
	Connection con;
	ResultSet res;
	int startingIndex, uptoWhichRecord, offset, lastIndex, fromIndexForEmail, endIndexForEmail;
	ArrayList<SendMessageBatchRequestEntry> queRequestList = new ArrayList<SendMessageBatchRequestEntry>();
	public AmazonSQS sqs;
	String queUrl, usersData = "", dataId = "", category = "";
	QueueModelObject queueModelObject = null;

	// for populatiiong data into cloudsearch for jobs.
	public DataPopulationThread(int offset, QueueModelObject model, Connection con) {
		// Creating thread with following value as their property..............
		// ==============================================================================================

		this.startingIndex = Integer.parseInt(model.getFrom());
		this.uptoWhichRecord = model.getUptoWhichUser();

		// ===============================//========================================
		this.fromIndexForEmail = Integer.parseInt(model.getFrom());
		this.endIndexForEmail = model.getUptoWhichUser();
		// =======================================//==============================================================
		this.offset = offset;
		this.lastIndex = this.startingIndex + this.offset;
		this.queUrl = model.getQueueUrl();
		this.queueModelObject = model;

		try {
			// creating connection with mysql...........
			if (Utility.con == null)
				Utility.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			this.st = (Statement) Utility.con.createStatement();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.sqs = new AmazonSQSClient(Utility.credentials);
		this.sqs.setRegion(Utility.queueRegion);

		try {
			// creating connection with mysql...........
			if (Utility.con == null)
				Utility.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			this.st = (Statement) Utility.con.createStatement();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Utility.threadName++;
		System.out.println("Datafor this thread=" + this.fromIndexForEmail + " , " + this.endIndexForEmail + " quueu url= " + model.getQueueUrl());
		// this.thread.start();
		// insertingUser_DataInQueue(this);
		// ==============================================================================================

	}

	public void insertingUser_DataInQueue(DataPopulationThread jobserious) {

		// ==============================================================================================

		String q = "";
		if (queueModelObject.getProcessType().toLowerCase().contains("web") && !queueModelObject.getProcessName().toLowerCase().contains("handpicked")) {
			q = this.queueModelObject.getQuery() + ""
			// + " AND ( " +
			// this.queueModelObject.getInvalidDomainString() +
			// " )"
					+ " order by t1.id desc limit " + jobserious.startingIndex + "," + jobserious.offset;
		} else if (queueModelObject.getProcessName().equalsIgnoreCase("Oakjob") && (!this.queueModelObject.getProcessType().contains("daily"))) {

			q = this.queueModelObject.getQuery()
			// " AND ( " +
			// this.queueModelObject.getInvalidDomainString() +
			// " ) "
					+ " order by open_at desc limit " + jobserious.startingIndex + "," + jobserious.offset;

		} else {
			q = this.queueModelObject.getQuery()
			// + " AND ( " +
			// this.queueModelObject.getInvalidDomainString() +
			// " ) "
					+ " limit " + jobserious.startingIndex + "," + jobserious.offset;
		}
		System.out.println(q);
		// ==============================================================================================

		try {
			// creating connection with mysql...........
			if (Utility.con == null)
				Utility.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// ==============================================================================================

		if (Utility.con != null) {
			try {
				if (jobserious.st == null)
					jobserious.st = (Statement) Utility.con.createStatement();
				jobserious.res = jobserious.st.executeQuery(q);
				if (jobserious.queRequestList.size() != 0)
					jobserious.queRequestList.clear();
				int count = 1;
				// fetching messages from resultset.................
				while (!jobserious.res.isClosed() && jobserious.res.next()) {
					// checking whether message size greater that
					// 125...........and uploading message in the queueu

					if (count <= Utility.usersPerMessage) {

						try {
							CloudSearchModel object = new CloudSearchModel();

							object = iniTializeCloudModelObject_ForExtraParameter(this.queueModelObject, object);

							this.dataId = object.id;

							String duplicateUserEmailObject = null;
							Object oakjobWebmaxAlerts = null;
							try {
								// getting existing object users email from
								// memcache
								// so that we can check duplicate mails
								if (this.queueModelObject.getProcessType().toLowerCase().contains("web")) {
									// first we check for the email

									duplicateUserEmailObject = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));
									try {
										if (Integer.parseInt(duplicateUserEmailObject) < 10) {

											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											oakjobWebmaxAlerts = Utility.memcacheObj.get(memKey);

										} else {
											oakjobWebmaxAlerts = "After ten";

										}
									} catch (Exception e) {
										// TODO Auto-generated catch block
										duplicateUserEmailObject = null;
									}

								} else {
									duplicateUserEmailObject = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));
									try {
										oakjobWebmaxAlerts = "other than web users";
										int value = Integer.parseInt(duplicateUserEmailObject);
									} catch (Exception e) {
										// TODO: handle exception
										duplicateUserEmailObject = null;
									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							// duplicate check... it means user has not been
							// processed at all

							if (duplicateUserEmailObject == null || oakjobWebmaxAlerts == null) {

								try {
									// searchAllJobsSingle(userDataObject);

									// check email,keyword,zipcode for user job
									// search process..

									if (this.queueModelObject.getProcessName().toLowerCase().contains("handpicked") && !object.getEmail().equalsIgnoreCase("") && !object.getEmail().equalsIgnoreCase("null")) {

										try {
											Utility.userProcessingCount.incrementAndGet();
											this.usersData = this.usersData + checkAscii_charater(object.toString());
											this.dataId = object.id;
											count++;
											String lastValues = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));

											if (lastValues == null) {
												lastValues = "1";
											} else {
												try {
													lastValues = String.valueOf(Integer.parseInt(lastValues) + 1);
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													lastValues = "1";
												}
											}
											// System.out.println(object.email.trim().toString()
											// + " lastvalues==" + lastValues);

											Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString(), 0, lastValues);
											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											try {
												Utility.memcacheObj.set(memKey, 0, "0");
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
											}

										} catch (Exception e1) {
											// TODO Auto-generated catch
											// block
											e1.printStackTrace();
										}

									}

									else if (!object.email.equalsIgnoreCase("")
									// && !object.keyword.equalsIgnoreCase("")
											&& !object.zipcode.equalsIgnoreCase("")) {
										// if user dont have lat, long then we
										// will
										// process them from api..
										try {
											Utility.userProcessingCount.incrementAndGet();
											this.usersData = this.usersData + checkAscii_charater(object.toString());
											this.dataId = object.id;
											count++;
											String lastValues = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));

											if (lastValues == null) {
												lastValues = "1";
											} else {
												try {
													lastValues = String.valueOf(Integer.parseInt(lastValues) + 1);
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													lastValues = "1";
												}
											}
											// System.out.println(object.email.trim().toString()
											// + " lastvalues==" + lastValues);

											Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString(), 0, lastValues);
											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											try {
												Utility.memcacheObj.set(memKey, 0, "0");
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
											}

										} catch (Exception e1) {
											// TODO Auto-generated catch
											// block
											e1.printStackTrace();
										}

									} else {
										Utility.usersNotProcessed.getAndIncrement();
									}
								} catch (Exception e) {
									// TODO: handle exception
								}

							} else {

								Utility.duplicateUsers++;
								try {
									Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + Utility.duplicateUserKey, 0, String.valueOf(Utility.duplicateUsers));
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						try {
							// make a request for the add message in the reqest
							// list...........

							// Fetching that current resultset
							// users..........==============================//=================================

							CloudSearchModel object = new CloudSearchModel();
							object = iniTializeCloudModelObject_ForExtraParameter(this.queueModelObject, object);
							this.dataId = object.id;
							// =====================================================================================================

							String duplicateUserEmailObject = null;
							Object oakjobWebmaxAlerts = null;
							try {
								// getting existing object users email from
								// memcache
								// so that we can check duplicate mails
								if (this.queueModelObject.getProcessType().toLowerCase().contains("web")) {
									// first we check for the email

									duplicateUserEmailObject = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));
									try {
										if (Integer.parseInt(duplicateUserEmailObject) < 10) {

											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											oakjobWebmaxAlerts = Utility.memcacheObj.get(memKey);

										} else {
											oakjobWebmaxAlerts = "After ten";

										}
									} catch (Exception e) {
										// TODO Auto-generated catch block
										duplicateUserEmailObject = null;
									}

								} else {
									duplicateUserEmailObject = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));
									try {
										oakjobWebmaxAlerts = "other than web users";
										int value = Integer.parseInt(duplicateUserEmailObject);
									} catch (Exception e) {
										// TODO: handle exception
										duplicateUserEmailObject = null;
									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							// duplicate check... it means user has not been
							// processed at all

							if (duplicateUserEmailObject == null || oakjobWebmaxAlerts == null) {

								try {
									// searchAllJobsSingle(userDataObject);

									// check email,keyword,zipcode for user job
									// search process..

									if (this.queueModelObject.getProcessName().toLowerCase().contains("handpicked") && !object.getEmail().equalsIgnoreCase("") && !object.getEmail().equalsIgnoreCase("null")) {

										try {
											Utility.userProcessingCount.incrementAndGet();
											this.usersData = this.usersData + checkAscii_charater(object.toString());
											this.dataId = object.id;
											count++;
											String lastValues = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));

											if (lastValues == null) {
												lastValues = "1";
											} else {
												try {
													lastValues = String.valueOf(Integer.parseInt(lastValues) + 1);
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													lastValues = "1";
												}
											}
											// System.out.println(object.email.trim().toString()
											// + " lastvalues==" + lastValues);

											Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString(), 0, lastValues);
											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											try {
												Utility.memcacheObj.set(memKey, 0, "0");
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
											}

										} catch (Exception e1) {
											// TODO Auto-generated catch
											// block
											e1.printStackTrace();
										}

									}
									// for other process blaock now we are
									// processeing keyword missing users with
									// j2c api
									else if (!object.email.equalsIgnoreCase("")
									// && !object.keyword.equalsIgnoreCase("")
											&& !object.zipcode.equalsIgnoreCase("")) {
										// if user dont have lat, long then we
										// will
										// process them from api..
										try {
											Utility.userProcessingCount.incrementAndGet();
											this.usersData = this.usersData + checkAscii_charater(object.toString());
											this.dataId = object.id;
											count++;
											String lastValues = String.valueOf(Utility.memcacheObj.get(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString()));

											if (lastValues == null) {
												lastValues = "1";
											} else {
												try {
													lastValues = String.valueOf(Integer.parseInt(lastValues) + 1);
												} catch (Exception e) {
													// TODO Auto-generated catch
													// block
													lastValues = "1";
												}
											}
											// System.out.println(object.email.trim().toString()
											// + " lastvalues==" + lastValues);

											Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + object.email.trim().toString(), 0, lastValues);
											String memKey = object.getEmail() + object.keyword + object.zipcode;
											memKey = memKey.replaceAll("\\s", "");
											try {
												Utility.memcacheObj.set(memKey, 0, "0");
											} catch (Exception e) {
												// TODO Auto-generated catch
												// block
											}

										} catch (Exception e1) {
											// TODO Auto-generated catch
											// block
											e1.printStackTrace();
										}

									} else {
										Utility.usersNotProcessed.getAndIncrement();

									}
								} catch (Exception e) {
									// TODO: handle exception
								}

							} else {

								Utility.duplicateUsers++;

								try {
									Utility.memcacheObj.set(this.queueModelObject.getProcessName().replaceAll(" ", "") + "_" + Utility.duplicateUserKey, 0, String.valueOf(Utility.duplicateUsers));
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							// /===============Adding them to the
							// queueu====================================

							SendMessageBatchRequestEntry requestEntry = new SendMessageBatchRequestEntry();
							requestEntry.setId(jobserious.dataId);
							requestEntry.setMessageBody(jobserious.usersData.toString());
							jobserious.queRequestList.add(requestEntry);

							jobserious.usersData = "";
							jobserious.dataId = "";
							requestEntry = null;

							if (jobserious.queRequestList.size() >= 10) {
								// we just want to process the data and not to
								// send
								// it in the queueu

								SendMessageBatchResult sendMessageBatch = jobserious.sqs.sendMessageBatch(jobserious.queUrl, jobserious.queRequestList);
								System.out.println("Data send" + jobserious.queRequestList.size());
								Utility.totalMessage = Utility.totalMessage + jobserious.queRequestList.size();

								if (sendMessageBatch.getFailed().size() != 0) {
									System.out.println(new Gson().toJson(jobserious.queRequestList));
								}

								jobserious.queRequestList.clear();

							}
							count = 1;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
				// ==============================================================================================

				if (count > 1) {

					// checking whether there is some message is still
					// left..................
					SendMessageBatchRequestEntry requestEntry = new SendMessageBatchRequestEntry();
					requestEntry.setId(jobserious.dataId);
					requestEntry.setMessageBody(jobserious.usersData.toString());

					if (jobserious.queRequestList.size() <= 10) {
						// we just want to process the data and not to send
						// it in the queueu

						jobserious.queRequestList.add(requestEntry);
						SendMessageBatchResult sendMessageBatch = jobserious.sqs.sendMessageBatch(jobserious.queUrl, jobserious.queRequestList);

						if (sendMessageBatch.getFailed().size() != 0) {
							System.out.println(new Gson().toJson(jobserious.queRequestList));
						}
						System.out.println("Data send" + jobserious.queRequestList.size());
						Utility.totalMessage = Utility.totalMessage + jobserious.queRequestList.size();
						if (sendMessageBatch.getFailed().size() != 0) {
							System.out.println(new Gson().toJson(jobserious.queRequestList));
						}
						jobserious.queRequestList.clear();

						jobserious.usersData = "";
						jobserious.dataId = "";
						requestEntry = null;

					} else {

						// we just want to process the data and not to send
						// it in the queueu

						jobserious.queRequestList.clear();
						jobserious.queRequestList.add(requestEntry);

						SendMessageBatchResult sendMessageBatch = jobserious.sqs.sendMessageBatch(jobserious.queUrl, jobserious.queRequestList);
						try {
							Thread.sleep(0);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						System.out.println("Data send" + jobserious.queRequestList.size());

						if (sendMessageBatch.getFailed().size() != 0) {
							System.out.println(new Gson().toJson(jobserious.queRequestList));
						}
						jobserious.queRequestList.clear();

						usersData = "";
						dataId = "";
						requestEntry = null;

					}
					count = 1;

				}

				else if (count == 1 && jobserious.queRequestList.size() != 0) {
					// we just want to process the data and not to send
					// it in the queueu

					SendMessageBatchRequestEntry requestEntry = new SendMessageBatchRequestEntry();
					requestEntry.setId(jobserious.dataId);
					requestEntry.setMessageBody(jobserious.usersData.toString());
					SendMessageBatchResult sendMessageBatch = jobserious.sqs.sendMessageBatch(jobserious.queUrl, jobserious.queRequestList);
					System.out.println("Data send" + jobserious.queRequestList.size());
					if (sendMessageBatch.getFailed().size() != 0) {
						System.out.println(new Gson().toJson(jobserious.queRequestList));
					}
					jobserious.queRequestList.clear();

					usersData = "";
					dataId = "";
					requestEntry = null;

				}
				count = 1;

				// ==============================================================================================

				if (jobserious.lastIndex >= jobserious.uptoWhichRecord) {
					Utility.threadCompled++;
					// System.out.println(
					// "Thread breaks " + jobserious.thread.getName() + "number
					// of breaks =" + Utility.threadCompled);
					if (Utility.threadCompled >= Utility.numberOfThread) {

						Utility.finalMailText += makeMailHtml();
						if (!MainClass.queueCountString.contains(this.queueModelObject.getQueueName())) {
							Utility.totalNumberOfQueueus.getAndIncrement();
							// Utility.totalNumberOfqueue_temp.getAndIncrement();

							MainClass.queueCountString += this.queueModelObject.getQueueName() + " # ";
						}

						// System.out.println("send mail");

						System.out.println("Total data in database=" + Utility.totalUsersInDb);
						System.out.println("total data in queu=" + Utility.userProcessingCount.get());
						System.out.println("Total data in queu with less info=" + Utility.usersNotProcessed.get());
						System.out.println("Duplicate users=" + Utility.duplicateUsers);

						// ===========================Tabulo users stats code
						// will goes here=================
						ProcessWiseUserModel lastProcessModel = null;
						if (this.queueModelObject.getQueueName().contains("evening")) {

							lastProcessModel = Utility.processWiseUserstats_map.get(this.queueModelObject.getProcessName() + "_evening");
						} else {
							lastProcessModel = Utility.processWiseUserstats_map.get(this.queueModelObject.getProcessName());
						}

						if (lastProcessModel == null) {
							lastProcessModel = new ProcessWiseUserModel();
							lastProcessModel.setProcessWiseStats(new HashMap<String, Integer>());

						}
						if (this.queueModelObject.getProcessType().contains("daily")) {
							int lastUserProcess = 0;
							if (lastProcessModel.getProcessWiseStats().get("daily") != null) {
								lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("daily");
							} else {
								lastUserProcess = Utility.userProcessingCount.get();
							}

							lastProcessModel.getProcessWiseStats().put("daily", lastUserProcess);
						}
						if (this.queueModelObject.getProcessType().contains("opener")) {
							int lastUserProcess = 0;
							if (lastProcessModel.getProcessWiseStats().get("opener") != null) {
								lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("opener");
							} else {
								lastUserProcess = Utility.userProcessingCount.get();
							}

							lastProcessModel.getProcessWiseStats().put("opener", lastUserProcess);
						}
						if (this.queueModelObject.getProcessType().contains("clicker")) {
							int lastUserProcess = 0;
							if (lastProcessModel.getProcessWiseStats().get("clicker") != null) {
								lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("clicker");
							} else {
								lastUserProcess = Utility.userProcessingCount.get();
							}

							lastProcessModel.getProcessWiseStats().put("clicker", lastUserProcess);
						}
						if (this.queueModelObject.getProcessType().contains("weekly")) {
							int lastUserProcess = 0;
							if (lastProcessModel.getProcessWiseStats().get("weekly") != null) {
								lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("weekly");
							} else {
								lastUserProcess = Utility.userProcessingCount.get();
							}
							lastProcessModel.getProcessWiseStats().put("weekly", lastUserProcess);
						}
						if (this.queueModelObject.getProcessType().contains("monthly")) {
							int lastUserProcess = 0;
							if (lastProcessModel.getProcessWiseStats().get("monthly") != null) {
								lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("monthly");
							} else {
								lastUserProcess = Utility.userProcessingCount.get();
							}
							lastProcessModel.getProcessWiseStats().put("monthly", lastUserProcess);
						}
						// if
						// (this.queueModelObject.getProcessType().contains("web"))
						// {
						// int lastUserProcess = 0;
						// if (lastProcessModel.getProcessWiseStats().get("web")
						// != null) {
						// lastUserProcess = Utility.userProcessingCount.get()
						// + lastProcessModel.getProcessWiseStats().get("web");
						// }
						// else {
						// lastUserProcess = Utility.userProcessingCount.get();
						// }
						// lastProcessModel.getProcessWiseStats().put("web",
						// lastUserProcess);
						// }
						// setting value back to the hashmap
						if (this.queueModelObject.getQueueName().contains("evening")) {
							Utility.processWiseUserstats_map.put(this.queueModelObject.getProcessName() + "_evening", lastProcessModel);

						} else {
							Utility.processWiseUserstats_map.put(this.queueModelObject.getProcessName(), lastProcessModel);

						}
						// ==========================================================
						// write queusize in the
						// file.............................
						try {
							Utility.writeSizeIntoFile_forCombineProcess(Utility.userProcessingCount.get(), this.queueModelObject);

							if (MainClass.queueName_forWhichIsMade == null) {
								Utility.write_domainWise_user_Stats_infile(this.queueModelObject);
								Utility.write_processWise_user_Stats_infile(this.queueModelObject);
								Utility.write_ivalid_domain_wise_users_inprocess(this.queueModelObject);

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}

				// ==============================================================================================

				else {

					// changing the value of starting index and last
					// index.........................

					jobserious.startingIndex = jobserious.lastIndex;
					if ((jobserious.startingIndex + jobserious.offset) > jobserious.uptoWhichRecord) {
						jobserious.offset = jobserious.uptoWhichRecord - jobserious.lastIndex;
						jobserious.lastIndex = jobserious.startingIndex + jobserious.offset;
					} else {
						jobserious.lastIndex = jobserious.startingIndex + jobserious.offset;

					}
					jobserious.res = null;
					if (jobserious.lastIndex <= jobserious.uptoWhichRecord) {
						// calling method again with new limit
						// parameter.........................
						try {
							insertingUser_DataInQueue(jobserious);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {

						// complete data has been populated in the queue for
						// that paricular thread.

						Utility.threadCompled++;
						// System.out.println("Thread breaks " +
						// jobserious.thread.getName() + "number of breaks ="
						// + Utility.threadCompled);
						if (Utility.threadCompled >= Utility.numberOfThread) {

							Utility.finalMailText += makeMailHtml();
							if (!MainClass.queueCountString.contains(this.queueModelObject.getQueueName())) {
								Utility.totalNumberOfQueueus.getAndIncrement();
								// Utility.totalNumberOfqueue_temp.getAndIncrement();

								MainClass.queueCountString += this.queueModelObject.getQueueName() + " # ";
							}

							System.out.println("Total data in database=" + Utility.totalUsersInDb);
							System.out.println("total data in queu=" + Utility.userProcessingCount.get());
							System.out.println("Total data in queu with less info=" + Utility.usersNotProcessed.get());
							System.out.println("Duplicate users=" + Utility.duplicateUsers);

							// ===========================Tabulo users stats
							// code
							// will goes here=================
							ProcessWiseUserModel lastProcessModel = null;
							if (this.queueModelObject.getQueueName().contains("evening")) {

								lastProcessModel = Utility.processWiseUserstats_map.get(this.queueModelObject.getProcessName() + "_evening");
							} else {
								lastProcessModel = Utility.processWiseUserstats_map.get(this.queueModelObject.getProcessName());
							}

							if (lastProcessModel == null) {
								lastProcessModel = new ProcessWiseUserModel();
								lastProcessModel.setProcessWiseStats(new HashMap<String, Integer>());

							}
							if (this.queueModelObject.getProcessType().contains("daily")) {
								int lastUserProcess = 0;
								if (lastProcessModel.getProcessWiseStats().get("daily") != null) {
									lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("daily");
								} else {
									lastUserProcess = Utility.userProcessingCount.get();
								}

								lastProcessModel.getProcessWiseStats().put("daily", lastUserProcess);
							}
							if (this.queueModelObject.getProcessType().contains("opener")) {
								int lastUserProcess = 0;
								if (lastProcessModel.getProcessWiseStats().get("opener") != null) {
									lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("opener");
								} else {
									lastUserProcess = Utility.userProcessingCount.get();
								}

								lastProcessModel.getProcessWiseStats().put("opener", lastUserProcess);
							}
							if (this.queueModelObject.getProcessType().contains("clicker")) {
								int lastUserProcess = 0;
								if (lastProcessModel.getProcessWiseStats().get("clicker") != null) {
									lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("clicker");
								} else {
									lastUserProcess = Utility.userProcessingCount.get();
								}

								lastProcessModel.getProcessWiseStats().put("clicker", lastUserProcess);
							}
							if (this.queueModelObject.getProcessType().contains("weekly")) {
								int lastUserProcess = 0;
								if (lastProcessModel.getProcessWiseStats().get("weekly") != null) {
									lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("weekly");
								} else {
									lastUserProcess = Utility.userProcessingCount.get();
								}
								lastProcessModel.getProcessWiseStats().put("weekly", lastUserProcess);
							}
							if (this.queueModelObject.getProcessType().contains("monthly")) {
								int lastUserProcess = 0;
								if (lastProcessModel.getProcessWiseStats().get("monthly") != null) {
									lastUserProcess = Utility.userProcessingCount.get() + lastProcessModel.getProcessWiseStats().get("monthly");
								} else {
									lastUserProcess = Utility.userProcessingCount.get();
								}
								lastProcessModel.getProcessWiseStats().put("monthly", lastUserProcess);
							}
							// if
							// (this.queueModelObject.getProcessType().contains("web"))
							// {
							// int lastUserProcess = 0;
							// if
							// (lastProcessModel.getProcessWiseStats().get("web")
							// != null) {
							// lastUserProcess =
							// Utility.userProcessingCount.get()
							// +
							// lastProcessModel.getProcessWiseStats().get("web");
							// }
							// else {
							// lastUserProcess =
							// Utility.userProcessingCount.get();
							// }
							// lastProcessModel.getProcessWiseStats().put("web",
							// lastUserProcess);
							// }
							// setting value back to the hashmap
							if (this.queueModelObject.getQueueName().contains("evening")) {
								Utility.processWiseUserstats_map.put(this.queueModelObject.getProcessName() + "_evening", lastProcessModel);

							} else {
								Utility.processWiseUserstats_map.put(this.queueModelObject.getProcessName(), lastProcessModel);

							} // write queusize in the
								// file.............................
							Utility.write_domainWise_user_Stats_infile(this.queueModelObject);
							Utility.write_processWise_user_Stats_infile(this.queueModelObject);
							Utility.writeSizeIntoFile_forCombineProcess(Utility.userProcessingCount.get(), this.queueModelObject);

							// try {
							//
							// if (MainClass.queueName_forWhichIsMade == null
							// && Utility.numberOfActiveQueueCount > 25) {
							// Utility.write_domainWise_user_Stats_infile(this.queueModelObject);
							// Utility.write_processWise_user_Stats_infile(this.queueModelObject);
							// Utility.writeSizeIntoFile_forCombineProcess(
							// Utility.userProcessingCount.get(),
							// this.queueModelObject);
							//
							// }
							// }
							// catch (Exception e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
						}
					}
				}

				// ==============================================================================================

				try {

				} catch (Exception e) { // TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (Exception e) { // TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(q);

			}

		}
	}

	public CloudSearchModel iniTializeCloudModelObject_ForExtraParameter(QueueModelObject queueModelObject, CloudSearchModel dataModel) {
		try {
			dataModel.id = res.getString("id");
			// dataModel.dataId = dataModel.id;
		} catch (Exception e) {
			dataModel.id = "";
		}
		try {

			// here we add the search keyword with the mappping
			// keywords

			// if (Utility.isForOakjobProcess) {

			try {
				dataModel.keyword = res.getString("keyword").trim().replaceAll("'", "");
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				dataModel.keyword = "";
			}
			if (dataModel.keyword.contains("/")) {

				dataModel.keyword = dataModel.keyword.replaceAll("/", " / ");

			}
			if (dataModel.keyword.contains("-")) {

				dataModel.keyword = dataModel.keyword.replaceAll("-", " / ");

			}

			try {

				dataModel.keyword = dataModel.keyword.replaceAll("\\|", " ").replaceAll("\n", " ");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// checking for the keyword wor mak 20 character
			// length checking for sherif and police as well
			if (dataModel.keyword.toLowerCase().contains("sheriff") || dataModel.keyword.toLowerCase().contains("police")) {
				dataModel.keyword = "";
				Utility.invalidUsers++;
			}
			String spaceWiseKeyword[] = null;
			if (!dataModel.keyword.equalsIgnoreCase("")) {
				spaceWiseKeyword = dataModel.keyword.split(" ");
				for (int i = 0; i < spaceWiseKeyword.length; i++) {

					if (spaceWiseKeyword[i].length() > 20) {
						dataModel.keyword = "";
						Utility.invalidUsers++;
						break;

					}

				}
			}
			if (!dataModel.getKeyword().equalsIgnoreCase("")) {

				char c = dataModel.getKeyword().toUpperCase().charAt(0);
				// getting search keyword from the mapping table
				ArrayList<MapKeyWordModel> list = null;
				// if
				// (this.queueModelObject.getProcessName().equalsIgnoreCase("senzajobalerts")
				// ||
				// this.queueModelObject.getProcessName().equalsIgnoreCase("clearfit")||
				// this.queueModelObject.getProcessName().equalsIgnoreCase("geeboalerts")
				// ||
				// this.queueModelObject.getProcessName().equalsIgnoreCase("jobvitals")
				// ||
				// this.queueModelObject.getProcessName().equalsIgnoreCase("cylcon"))
				// {
				//
				// list = Utility.oak_job_keyword_mapping_map
				// .get(String.valueOf(c));
				//
				// }
				// else {
				list = Utility.oak_job_keyword_mapping_map.get(String.valueOf(c));

				// }

				if (list == null) {
					dataModel.searchKeyword = dataModel.keyword;
				} else {
					for (int i = 0; i < list.size(); i++) {
						// now we are cheking for the equal of keyword with the
						// mapping keyword 11-08-2016 done by pawan
						if (dataModel.getKeyword().toLowerCase().equalsIgnoreCase(list.get(i).getKeyword().toLowerCase())) {
							dataModel.searchKeyword = list.get(i).getSearchKeyword();
							Utility.searchKeyword_found++;

							// ==============================================//=====================================
							break;
						}
					}
					if (dataModel.getSearchKeyword().equalsIgnoreCase("")) {
						dataModel.searchKeyword = dataModel.keyword;
					}

				}
			}

			// =============================special handling for the nurse
			// keyword=========================================
			try {
				boolean flag = false;
				String RnSpecialHandlingString = "Nurse/RN/LPN/CRNA/LVN/r.n/l.p.n/c.r.n.a/l.v.n/rpn/r.p.n";
				String RnSpecialHandlingStringArray[] = RnSpecialHandlingString.split("\\/");
				for (int i = 0; i < RnSpecialHandlingStringArray.length; i++) {

					if (flag)
						break;
					if (spaceWiseKeyword != null) {
						for (int j = 0; j < spaceWiseKeyword.length; j++) {

							if (spaceWiseKeyword[j].contains(RnSpecialHandlingStringArray[i])) {

								dataModel.searchKeyword = RnSpecialHandlingString;
								Utility.nurseWiseKeywordsUsersCount++;
								flag = true;
								break;
							}
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// here we are cheking for the pipi in the users keyword and
			// replacing it with the special character in alerts we are doing
			// vice versa

			if (dataModel.keyword.contains("|") || dataModel.searchKeyword.contains("|")) {

				boolean flag = true;

				while (flag) {
					dataModel.keyword = dataModel.keyword.replace("|", "$pipe$");
					dataModel.searchKeyword = dataModel.searchKeyword.replace("|", "$pipe$");
					if (dataModel.keyword.contains("|") || dataModel.searchKeyword.contains("|")) {

						dataModel.keyword = dataModel.keyword.replace("|", "$pipe$");
						dataModel.searchKeyword = dataModel.searchKeyword.replace("|", "$pipe$");

					} else {
						flag = false;
					}
				}

				Utility.pipeContainingKeywordsUsers++;

			}

			// =====================================================//====================================================

		} catch (Exception e) {
			e.printStackTrace();
			dataModel.keyword = "";
		}
		try {
			dataModel.email = res.getString("email").trim().replaceAll("\\|", "").replaceAll("\n", "");

			dataModel.email = dataModel.email.replace(" ", "").trim();
		} catch (Exception e) {
			dataModel.email = "";
			// jobvitals web alerts email is replaces by email
			// address in db
			try {
				dataModel.email = res.getString("EmailAddress").replaceAll("\\|", "").replaceAll("\n", "");
				dataModel.email = dataModel.email.replace(" ", "");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				dataModel.email = "";
			}
		}

		try {
			dataModel.zipcode = res.getString("zipcode").replaceAll("\\|", " ").replaceAll("\n", " ");
			;

			if (dataModel.zipcode == null || dataModel.zipcode.toLowerCase().contains("null"))
				dataModel.zipcode = "";
			dataModel.zipcode = dataModel.zipcode.trim();

		} catch (Exception e) {
			dataModel.zipcode = "";
			try {
				// jobvitals web alerts zipcode is replaces by
				// zip
				// address in db
				dataModel.zipcode = res.getString("location").replaceAll("\\|", " ").replaceAll("\n", " ");
				if (dataModel.zipcode.toLowerCase().contains("null"))
					dataModel.zipcode = "";
			} catch (Exception e2) {
				// TODO: handle exception
				dataModel.zipcode = "";
				try {
					dataModel.zipcode = res.getString("zip").replaceAll("\\|", " ").replaceAll("\n", " ");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					dataModel.zipcode = "";

				}
				if (dataModel.zipcode != null && dataModel.zipcode.toLowerCase().contains("null"))
					dataModel.zipcode = "";
			}
		}

		// for thoose users table having both columb zipcode and
		// location
		try {
			if (dataModel.zipcode != null && dataModel.zipcode.equalsIgnoreCase("")) {
				try {
					dataModel.zipcode = res.getString("location");
					if (dataModel.zipcode.toLowerCase().contains("null"))
						dataModel.zipcode = "";
				} catch (Exception e2) {
					// TODO: handle exception
					dataModel.zipcode = "";
				}
			}
		} catch (Exception e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		try {
			int zipString = Integer.parseInt(dataModel.zipcode);

			if (String.valueOf(zipString).length() >= 6) {
				// we are making it invalid users because its zip code lenggth
				// is greater that 6
				dataModel.email = "";
			}

		} catch (Exception e) {
			try {
				// TODO Auto-generated catch block
				// spliting zip String to get the zipcode
				if (dataModel.zipcode.contains(",")) {
					String zipString[] = dataModel.zipcode.split(",");

					String city = "", state = "";
					try {
						city = zipString[0].replaceAll("\\|", " ").replaceAll("\n", " ");
					} catch (Exception e2) {
						// TODO Auto-generated catch block
					}
					try {
						state = zipString[1].replaceAll("\\|", " ").replaceAll("\n", " ");
					} catch (Exception e2) {
						// TODO Auto-generated catch block
					}

					ArrayList<String> data = Utility.getzipcodeLatLongFromCityAndState(city, state, Utility.con);
					if (data.size() > 0) {
						try {
							dataModel.zipcode = data.get(0);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							dataModel.city = data.get(1).replaceAll("\\|", " ").replaceAll("\n", " ");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							dataModel.state = data.get(2).replaceAll("\\|", " ").replaceAll("\n", " ");
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							dataModel.latitude = data.get(3);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							dataModel.longitude = data.get(4);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}

				} else {
					// FOR HANDPICKED USERS WE DONT WANT TO SEND THOSE USERS
					// WITH BAD ZIPCODE
					if (this.queueModelObject.getProcessName().toLowerCase().contains("handpicked")) {

						if (dataModel.keyword == null || dataModel.keyword.equalsIgnoreCase("")) {
							dataModel.email = "";

						}

					}
					// else {
					// dataModel.email = "";
					// }

				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}
		}

		try {
			dataModel.advertiseMent = res.getString("advertisement").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
			;
		} catch (Exception e) {
			dataModel.advertiseMent = "";
		}
		try {
			if (dataModel.latitude.equalsIgnoreCase(""))
				dataModel.latitude = res.getString("latitude").trim();

			if (dataModel.latitude.toLowerCase().contains("null"))
				dataModel.latitude = "";
		} catch (Exception e) {
			dataModel.latitude = "";
		}
		try {
			if (dataModel.longitude.equalsIgnoreCase(""))
				dataModel.longitude = res.getString("longitude").trim();

			if (dataModel.longitude.toLowerCase().contains("null"))
				dataModel.longitude = "";
		} catch (Exception e) {
			dataModel.longitude = "";
		}
		try {
			dataModel.alert_id = res.getString("alert_id").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		} catch (Exception e) {
		}
		double latitude, longitude;
		Double R = (double) 6371; // earth radius in km
		double radius = 30; // km

		try {
			dataModel.radius = res.getDouble("radius");
		} catch (Exception e) {
			dataModel.radius = radius;
		}
		try {
			dataModel.frequency = res.getString("frequency").trim();
		} catch (Exception e) {
			dataModel.frequency = "";
		}

		try {
			latitude = Double.parseDouble(dataModel.latitude);
		} catch (Exception e) {
			latitude = 0;
		}

		try {
			longitude = Double.parseDouble(dataModel.longitude);
		} catch (Exception e) {
			longitude = 0;
		}

		try {
			if (latitude != 0 && longitude != 0) {
				// real
				// get the upper and lower bounds lat-long
				dataModel.upperLatitude = latitude + Math.toDegrees(dataModel.radius / R);
				dataModel.upperLongitude = longitude - Math.toDegrees(dataModel.radius / R / Math.cos(Math.toRadians(latitude)));
				dataModel.lowerLongitude = longitude + Math.toDegrees(dataModel.radius / R / Math.cos(Math.toRadians(latitude)));
				dataModel.lowerLatitude = latitude - Math.toDegrees(dataModel.radius / R);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// here we check for the 70 miles of ups-new feed
		// zipcode=40202

		double disBetweenTwoZipCode = 0;
		if (dataModel.latitude != null && !dataModel.latitude.equalsIgnoreCase("") && dataModel.longitude != null && !dataModel.longitude.equalsIgnoreCase("")) {

			disBetweenTwoZipCode = Utility.distFrom(dataModel.latitude, dataModel.longitude);

		}
		if (disBetweenTwoZipCode != 0 && disBetweenTwoZipCode <= 70) {

			// System.out.println(disBetweenTwoZipCode +
			// " distance is less than 70 miles " +
			// object.zipcode + " ,city" + object.city
			// + " ,state=" + object.state);
			dataModel.isDistanceLessThan70Miles = "YES";

		} else {
			dataModel.isDistanceLessThan70Miles = "NO";

		}

		// ==========================================///=====================

		try {
			if (dataModel.city.equalsIgnoreCase(""))
				dataModel.city = res.getString("city").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		} catch (Exception e) {
			dataModel.city = "";
		}
		try {
			if (dataModel.city.equalsIgnoreCase(""))
				dataModel.city = res.getString("primary_city").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
			;
		} catch (Exception e) {
			dataModel.city = "";
		}
		try {
			if (dataModel.state.equalsIgnoreCase(""))
				dataModel.state = res.getString("state").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		} catch (Exception e) {
			dataModel.state = "";
		}
		try {
			// using this check for the lead5 media api
			if (this.queueModelObject.getProcessType().toLowerCase().contains("open")) {

				// dataModel.click = res.getString("open").trim();
				dataModel.click = "1";

			}
			// indeed api special check
			else if (this.queueModelObject.getProcessType().toLowerCase().contains("click")) {
				dataModel.click = "2";
			} else {
				dataModel.click = "0";

			}
		} catch (Exception e) {
			try {
				// dataModel.click = res.getString("opener").trim();
				dataModel.click = "1";
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				dataModel.click = "1";
			}

		}

		try {
			dataModel.registrationDate = res.getString("date").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		} catch (Exception e) {

			try {
				dataModel.registrationDate = res.getString("current_date").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
			} catch (Exception e1) {
				// if no column found

				try {
					dataModel.registrationDate = res.getString("open_date").trim().replaceAll("\\|", " ").replaceAll("\n", " ");

				} catch (Exception e2) {

					try {
						dataModel.registrationDate = res.getString("Datetime").trim().replaceAll("\\|", " ").replaceAll("\n", " ");

					} catch (Exception e3) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						String date1 = sdf.format(new Date().getTime());
						dataModel.registrationDate = date1.toString();

					}

				}

			}

		}

		try {
			// this for the mornign alerts
			if (!queueModelObject.getProcessType().toLowerCase().contains("evening")) {
				// Calendar cal = Calendar.getInstance();
				Utility.lastSeconds_morning = Utility.lastSeconds_morning + Utility.secondsIntervalIn_schedule;
				if (Utility.lastSeconds_morning % 60 == 0) {
					Utility.lastSeconds_morning = 00;
					Utility.lastMin_morning = Utility.lastMin_morning + 1;
					if (Utility.lastMin_morning % 60 == 0) {
						Utility.lastMin_morning = 00;
						Utility.lasthour_morning = Utility.lasthour_morning + 1;
						if (Utility.lasthour_morning > Utility.uptoHourInterval_morning) {

							Utility.lasthour_morning = Utility.startHour_morning;
							Utility.lastMin_morning = 00;
							Utility.lastSeconds_morning = 00;

						}

					}
				}
			}
			// this code is done on 28-09-2016 for evenign alerts
			else {

				// Calendar cal = Calendar.getInstance();
				Utility.lastSeconds_evening = Utility.lastSeconds_evening + Utility.secondsIntervalIn_schedule;
				if (Utility.lastSeconds_evening % 60 == 0) {
					Utility.lastSeconds_evening = 00;
					Utility.lastMin_evening = Utility.lastMin_evening + 1;
					if (Utility.lastMin_evening % 60 == 0) {
						Utility.lastMin_evening = 00;
						Utility.lasthour_evening = Utility.lasthour_evening + 1;

						if (Utility.lasthour_evening > Utility.uptoHourInterval_evening) {
							Utility.lasthour_evening = Utility.startHour_evening;
							Utility.lastMin_evening = 00;
							Utility.lastSeconds_evening = 00;

						}
					}
				}

			}

			try {
				dataModel.zip_timezone = res.getString("zipcode_timezone").trim().replaceAll("\\|", " ").replaceAll("\n", " ");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				dataModel.zip_timezone = "";
			}

			// SETTING SCHEDUTE SEND FOR MAILGUN AND SENDRID
			// USERS
			// 2016-04-28 only for the oakjob by users timezone
			if (dataModel.zip_timezone != null && !dataModel.zip_timezone.equalsIgnoreCase("")) {
				// here we are doing the special handling for the PST AND GMT
				// TIMEZONE
				if (dataModel.zip_timezone.equalsIgnoreCase("pst")) {
					dataModel.zip_timezone = "GMT-08:00";
				}

				if (dataModel.zip_timezone.equalsIgnoreCase("GMT")) {
					dataModel.zip_timezone = "GMT-00:00";
				}

				if (!queueModelObject.getProcessType().toLowerCase().contains("evening")) {
					dataModel = getModelWithScheduleSendFor_Morning(dataModel);
				} else {
					// here we do the code for the timezoneClearfit

					dataModel = getModelWithScheduleSendFor_Evening(dataModel);
				}

			} else {
				if (!queueModelObject.getProcessType().toLowerCase().contains("evening")) {
					dataModel.scheduleSendforWhiteLabel = getScheduleSendForMailgun(Utility.lasthour_morning + ":" + Utility.lastMin_morning + ":" + Utility.lastSeconds_morning, "EST");
					dataModel.open_at = getOpentimeForSendGrid(Utility.lasthour_morning + ":" + Utility.lastMin_morning + ":" + Utility.lastSeconds_morning, "GMT-05");
				} else {

					dataModel.scheduleSendforWhiteLabel = getScheduleSendForMailgun(Utility.lasthour_evening + ":" + Utility.lastMin_evening + ":" + Utility.lastSeconds_evening, "EST");
					dataModel.open_at = getOpentimeForSendGrid(Utility.lasthour_evening + ":" + Utility.lastMin_evening + ":" + Utility.lastSeconds_evening, "GMT-05");

				}
			}

			// ===================================//===================================

		} catch (Exception e) {
			// schedule send for the mailgun process
			e.printStackTrace();
			dataModel.scheduleSendforWhiteLabel = "";
			dataModel.open_at = "";

		}

		// ================================first and last
		// name====================================

		try {
			dataModel.firstName = res.getString("first_name").trim().replaceAll("\n", " ").replaceAll("\\|", " ");
		} catch (Exception e) {

			try {
				dataModel.firstName = res.getString("name").trim().replaceAll("\n", " ").replaceAll("\\|", " ");
			} catch (Exception e1) {

				dataModel.firstName = "";

			}
		}
		try {
			dataModel.lastName = res.getString("last_name").trim().replaceAll("\n", " ").replaceAll("\\|", " ");
		} catch (Exception e) {
			dataModel.lastName = "";
		}
		try {
			dataModel.providerFromResultSet = res.getString("provider").trim().replaceAll("\n", " ").replaceAll("\\|", " ");
			if (dataModel.providerFromResultSet.toLowerCase().contains("null"))
				dataModel.providerFromResultSet = "";
		} catch (Exception e) {
			dataModel.providerFromResultSet = "";

		}

		try {
			dataModel.affiliate_code = res.getString("affiliate_code").replaceAll("\n", " ").replaceAll("\\|", " ");
		} catch (Exception e) {
			dataModel.affiliate_code = "";
		}

		// adding the country and main_provider in the queueu on 22-12-2016
		try {
			dataModel.main_provider = res.getString("main_provider").replaceAll("\\|", "").replaceAll("\n", " ");
		} catch (Exception e) {
			dataModel.main_provider = "";
		}
		try {
			dataModel.country = res.getString("country").replaceAll("\\|", "").replaceAll("\n", " ");
		} catch (Exception e) {
			dataModel.country = "";
		}

		// for elite only

		if (this.queueModelObject.getProcessName().equalsIgnoreCase("elitejob") && dataModel.affiliate_code.equalsIgnoreCase("105")) {
			Utility.uk_elite_users++;
		}

		dataModel.dashboradFilename = "";
		dataModel.userType = queueModelObject.getUserType().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.j2cT2Value = queueModelObject.getJ2cT2Value().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.jujuChannelValue = queueModelObject.getJujuChannelValue().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.liveRampPixelValue = queueModelObject.getLiveRampPixelValue().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.campaign_id = queueModelObject.getCampaign_id().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.domain_name = queueModelObject.getDomain_name().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.sendGridIpPool = queueModelObject.getSendGridIpPool().trim().replaceAll("\\|", " ");
		dataModel.pixelCategoryName = queueModelObject.getPixelCategoryName().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.sendGridCategory = queueModelObject.getSendGridCategory().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
		dataModel.white_label = queueModelObject.getWhite_label().trim().replaceAll("\\|", " ").replaceAll("\n", " ");

		// dataModel.postalAddress = queueModelObject.getPostalAddress().trim();
		// for the sendgrid and mailgun mapping key to get log the actual key in
		// the log
		// dated 10-08-2016
		dataModel.sendGridAndMailGunMappingKey = queueModelObject.getSendgridMappingKey().replaceAll("\\|", " ");

		// =============================================this is change in the
		// provider ========================================
		// main provider-subprovider-advertiesment-m/e

		dataModel.providerFromQueue = queueModelObject.getProviderFromQueue().trim().replaceAll("\\|", " ");

		// here we are appending web in the provider if processtype is we and we
		// dont found web from queue api
		if (queueModelObject.getProcessType().toLowerCase().contains("web") && !dataModel.providerFromQueue.toLowerCase().contains("web")) {
			dataModel.providerFromQueue = dataModel.providerFromQueue + "-web";
		}

		if (dataModel.providerFromResultSet == null || dataModel.providerFromResultSet.contains("null")) {
			dataModel.providerFromResultSet = "";
		}

		if (dataModel.advertiseMent == null || dataModel.advertiseMent.contains("null") || dataModel.advertiseMent.equalsIgnoreCase("0")) {
			dataModel.advertiseMent = "";
		}

		// CURRENTLY WE ARE DOING FOR ALL EXCEPT THE HANDPICKED lifecycle table
		// from 03-10-2016
		if (queueModelObject.getProcessName() != null) {

			if (dataModel.providerFromResultSet != null && !dataModel.providerFromResultSet.toLowerCase().contains("null") && !dataModel.providerFromResultSet.equalsIgnoreCase("") && !queueModelObject.getProcessType().toLowerCase().contains("web") && !dataModel.providerFromResultSet.equalsIgnoreCase(queueModelObject.getWhite_label())) {

				dataModel.providerFromQueue = dataModel.providerFromQueue + "-" + dataModel.providerFromResultSet;
			}
			if (dataModel.advertiseMent != null && !dataModel.advertiseMent.toLowerCase().contains("null") && !dataModel.advertiseMent.equalsIgnoreCase("") && !dataModel.advertiseMent.equalsIgnoreCase(queueModelObject.getWhite_label())) {

				dataModel.providerFromQueue = dataModel.providerFromQueue + "-" + dataModel.advertiseMent;
			}

			// we are appending affiliated code in provider for elitejob only
			if (queueModelObject.getProcessName().equalsIgnoreCase("elitejob") && !dataModel.affiliate_code.equalsIgnoreCase("")) {
				dataModel.providerFromQueue = dataModel.providerFromQueue + "-" + dataModel.affiliate_code;

			}

			if (queueModelObject.getProcessType() != null && !queueModelObject.getProcessType().equalsIgnoreCase("") && queueModelObject.getProcessType().toLowerCase().contains("evening")) {
				dataModel.providerFromQueue = dataModel.providerFromQueue + "-" + "evening";

			} else {
				dataModel.providerFromQueue = dataModel.providerFromQueue + "-" + "morning";
			}
		}
		// for handpicked subprovider sub ids handling
		setHandpicked_Subids(dataModel);

		// =========================================new code
		// ========================================
		dataModel.jujuChannelValue = Utility.channelWiseHashMap.get(dataModel.advertiseMent.trim().toLowerCase());
		dataModel.j2cT2Value = Utility.channelWiseHashMap.get(dataModel.advertiseMent.trim().toLowerCase());

		if (dataModel.j2cT2Value != null) {
			dataModel.j2cT2Value = "&t2=" + dataModel.j2cT2Value;
		}

		if (dataModel.jujuChannelValue == null || dataModel.j2cT2Value == null) {

			dataModel.jujuChannelValue = Utility.channelWiseHashMap.get(dataModel.white_label.trim().toLowerCase());
			dataModel.j2cT2Value = Utility.channelWiseHashMap.get(dataModel.white_label.trim().toLowerCase());

			if (dataModel.j2cT2Value != null) {
				dataModel.j2cT2Value = "&t2=" + dataModel.j2cT2Value;
			}
			if (dataModel.jujuChannelValue == null || dataModel.j2cT2Value == null) {
				Utility.channelNotFound += "\n" + this.queueModelObject.getProcessName() + "|" + dataModel.getAdvertiseMent();

				dataModel.j2cT2Value = queueModelObject.getJ2cT2Value().trim().replaceAll("\\|", " ").replaceAll("\n", " ");
				dataModel.jujuChannelValue = queueModelObject.getJujuChannelValue().trim().replaceAll("\\|", " ").replaceAll("\n", " ");

			}

		}
		//
		String key_421 = queueModelObject.getProcessName().split("\\|")[0].trim() + "-" + dataModel.getEmail().trim();
		if (Utility.map_with_421_error_users.get(key_421) != null && Utility.map_with_421_error_users.get(key_421) && !dataModel.email.equalsIgnoreCase("") && !dataModel.zipcode.equalsIgnoreCase("")) {
			// we are makign it as invalid users for the today queue processing

			Utility.users_with_421_error++;
			dataModel.email = "";
		}

		// ==================================code for the domain wise users
		// process stats
		try {
			// here we are doing special handling for the this user 27-12-2016
			if (dataModel.email != null && !dataModel.email.equalsIgnoreCase("") && dataModel.email.equalsIgnoreCase("klee@adjustersinternational.com")) {
				dataModel.email = "";
				Utility.invalidUsers++;

				Utility.sendEmailJustForAlerts("klee@adjustersinternational.com has been found in " + queueModelObject.getProcessName() + " type= " + queueModelObject.getProcessType());

				Utility.sendSms("klee@adjustersinternational.com has been found in " + queueModelObject.getProcessName() + " type= " + queueModelObject.getProcessType());

			} else {
				if (dataModel.email != null && !dataModel.email.equalsIgnoreCase("") && dataModel.email.contains("@") && dataModel.email.length() > 0) {

					String emailSplitedArray[] = dataModel.email.split("@");

					for (int i = 0; i < Utility.inValidDomain_blcok_keyword_list.size(); i++) {
						// here we are stopping the users having invalid domains
						if (emailSplitedArray[1].toLowerCase().contains(Utility.inValidDomain_blcok_keyword_list.get(i))) {

							if (queueModelObject.getProcessName().equalsIgnoreCase("rabbitalerts") && emailSplitedArray[1].toLowerCase().equalsIgnoreCase("westpost.net")) {

								// here we doing nothing because we have to
								// process the westpost.net user for
								// rabbitalerts only as suggested by jason
							} else {
								dataModel.email = "";
							}
							break;
						}

					}

					// here we are checking the gmail users minium 6 characer in
					// mail

					if (dataModel.email != "" && emailSplitedArray[1].toString().toLowerCase().contains("gmail")) {
						if (emailSplitedArray[0].length() <= 5) {
							// here we are making it
							dataModel.email = "";

						}
					}

					if ((emailSplitedArray.length > 1 && Utility.invalidDomainString.contains("|" + emailSplitedArray[1].trim() + "|")) || emailSplitedArray[1].trim().toLowerCase().contains(".gov") || emailSplitedArray[1].trim().toLowerCase().contains("law") || emailSplitedArray[1].trim().toLowerCase().contains("attorney") || emailSplitedArray[1].trim().toLowerCase().contains("rcmp") || emailSplitedArray[1].trim().toLowerCase().contains("police") || emailSplitedArray[1].trim().toLowerCase().contains("aegis")

					|| dataModel.keyword.trim().toLowerCase().contains("law") || dataModel.keyword.trim().toLowerCase().contains("attorney") || dataModel.keyword.trim().toLowerCase().contains("lawer") || emailSplitedArray[1].trim().toLowerCase().contains(".mil")) {

						dataModel.email = "";

					}

					else {
						if (emailSplitedArray.length > 1) {

							String emailDomain = emailSplitedArray[1];
							String domainWiseKey = "";

							if (this.queueModelObject.getDomain_name() != null && !this.queueModelObject.getDomain_name().equalsIgnoreCase("")) {
								domainWiseKey = this.queueModelObject.getDomain_name().trim();
							} else {
								domainWiseKey = "defaultDomain.com";
							}

							if (emailDomain.toLowerCase().contains("gmail")) {
								domainWiseKey = domainWiseKey + "_" + "gmail";

							} else if (emailDomain.toLowerCase().contains("yahoo")) {
								domainWiseKey = domainWiseKey + "_" + "yahoo";

							} else if (emailDomain.toLowerCase().contains("aol")) {
								domainWiseKey = domainWiseKey + "_" + "aol";

							} else {
								domainWiseKey = domainWiseKey + "_" + "others";

							}
							String key = "";
							if (this.queueModelObject.getQueueName().contains("evening")) {
								key = this.queueModelObject.getProcessName() + "-evening" + "_" + domainWiseKey.toLowerCase().trim();

							} else {
								key = this.queueModelObject.getProcessName() + "_" + domainWiseKey.toLowerCase().trim();

							}
							Integer lastCount = Utility.domainWiseUsersStats.get(key);
							if (lastCount == null) {
								lastCount = 1;
							} else {
								lastCount++;
							}
							Utility.domainWiseUsersStats.put(key, lastCount);
							// System.out.println(domainWiseKey.toLowerCase().trim()
							// +
							// " and last count=" + lastCount);
						}

					}
					if (dataModel.email != null && dataModel.email.equalsIgnoreCase("")) {
						Utility.invalidUsers++;
						Utility.invalid_domain_block_users++;
						// here we are creating a hash map for the for the
						// invalid domain wise users

						InvalidDomainModel lastModel = Utility.invalidDomain_users_count_map.get(this.queueModelObject.getProcessName().trim());
						if (lastModel != null) {
							Integer lastCount = lastModel.getInvalidMap().get(emailSplitedArray[1].toLowerCase().trim());
							if (lastCount != null)
								lastCount = lastCount + 1;
							else
								lastCount = 1;
							lastModel.getInvalidMap().put(emailSplitedArray[1].toLowerCase().trim(), lastCount);
						} else {
							lastModel = new InvalidDomainModel();
							lastModel.getInvalidMap().put(emailSplitedArray[1].toLowerCase().trim(), 1);

						}
						Utility.invalidDomain_users_count_map.put(this.queueModelObject.getProcessName().trim(), lastModel);

					}
				}
			}

		} catch (Exception e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}

		// =======================end of it============================

		// we are stopping sending alerts to canada users from 26-06-2017 as
		// suggestd by jason
		if (dataModel.country.equalsIgnoreCase("Canada")) {
			dataModel.email = "";
		}

		// try {
		// if (!dataModel.getAdvertiseMent().equalsIgnoreCase("0"))
		// creating_whitelable_domain_advertisement_wise_stats_model(dataModel,
		// queueModelObject);
		// } catch (Exception e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		return dataModel;

	}

	public String getOpentimeForSendGrid(String hourMintSec, String timezone) {
		String longTimeString = "";

		String formatString = Utility.sendgrid_currdate + " " + hourMintSec;
		Utility.scheduleSendDateFormatFOrBoth_sendgrid_Mailgun.setTimeZone(TimeZone.getTimeZone(timezone));

		// System.out.println(formatString);
		String scheduleSendDateFormat = "";

		Date gmtDateString;
		try {
			gmtDateString = Utility.gmtScheduleSendDateFormat.parse(formatString);

			scheduleSendDateFormat = Utility.scheduleSendDateFormatFOrBoth_sendgrid_Mailgun.format(gmtDateString);
			Date sendgridDate = new Date(scheduleSendDateFormat);

			long unixSeconds = ((sendgridDate.getTime()) / 1000);
			longTimeString = "" + unixSeconds;

			// System.out.println(longTimeString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return longTimeString;

	}

	public String getScheduleSendForMailgun(String hourTIme, String timeZone) {
		String formatString = Utility.sendgrid_currdate + " " + hourTIme;
		Utility.scheduleSendDateFormatFOrBoth_sendgrid_Mailgun.setTimeZone(TimeZone.getTimeZone(timeZone));

		// System.out.println(formatString);
		String scheduleSendDateFormat = "";

		Date gmtDateString;
		try {
			gmtDateString = Utility.gmtScheduleSendDateFormat.parse(formatString);

			scheduleSendDateFormat = Utility.scheduleSendDateFormatFOrBoth_sendgrid_Mailgun.format(gmtDateString);
			// System.out.println("mailgun" + scheduleSendDateFormat);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return scheduleSendDateFormat;
	}

	public String makeMailHtml() {

		String html = "<br /><table margin='10' width='500' height='40' style='background-image: url(http://www.backgroundsy.com/file/large/digital-waves-background.jpg);'>" + "<tr>" + "<td height='30' colspan='1'><center><b>" + this.queueModelObject.getProcessName() + " " + this.queueModelObject.getProcessType() + "</b></center></td>" + "</tr>" + "<tr>" + "<td   width='300' height='30'>Queue Name </td><td width='200' height='40'>" + this.queueModelObject.getQueueName() + "</td>" + "</tr>" + "<tr>" + "<td   width='300' height='30'>Total number of users in queue are</td><td width='200' height='40'>" + Utility.userProcessingCount.get() + "</td>" + "</tr><tr>" + "<td   width='300' height='30'>Total number of users in Database</td><td width='200' height='40'>" + Utility.totalUsersInDb
				+ "</td>" + "</tr><tr>" + "<td   width='300' height='30'>ToTal number of Duplicate user are</td><td   width='200' height='40'>" + Utility.duplicateUsers + "</td>" + "</tr><tr>" + "<td  width='300' height='30'>Total number of UserNotProcessd</td><td width='200' height='40'>" + Utility.usersNotProcessed.get() + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>From Index </td><td width='200' height='30'>" + fromIndexForEmail + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>End Index </td><td width='200' height='30'>" + endIndexForEmail + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>InvalidKeyword length users</td><td width='200' height='30'>" + Utility.invalidUsers + "</td></tr>" + "<tr>"
				+ "<td  width='300' height='30'>PipeContaingKeywords Users</td><td width='200' height='30'>" + Utility.pipeContainingKeywordsUsers + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>NurseWiseKeywords userscount | Elite uk users count:</td><td width='200' height='30'>" + Utility.nurseWiseKeywordsUsersCount + " | " + Utility.uk_elite_users + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>USER WITH 421 ERROR MSG</td><td width='200' height='30'>" + Utility.users_with_421_error + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>SEARCH KEYWORD FOUND USERS COUNT</td><td width='200' height='30'>" + Utility.searchKeyword_found + "</td></tr>" + "<tr>" + "<td  width='300' height='30'>INVALID DOMAIN BLOCK USERS COUNT</td><td width='200' height='30'>"
				+ Utility.invalid_domain_block_users + "</td></tr>" + "<tr>" + "<td   width='500' height='30' colspan='2'>Query for the Data base " + this.queueModelObject.getQuery() + "</td></tr>" + "</table>";

		return html;

	}

	public CloudSearchModel getModelWithScheduleSendFor_Evening(CloudSearchModel dataModel) {

		int lasthourForSendgird = 0, timeZoneGap = 0;
		// for evening scheduel send of 4 pm to 7 pm
		if (!dataModel.zip_timezone.contains("GMT-05") && !dataModel.zip_timezone.contains("GMT-5")) {
			String operator = "";

			if (dataModel.zip_timezone.contains("-")) {
				try {
					timeZoneGap = 5 - Integer.parseInt(dataModel.zip_timezone.split("-")[1].split(":")[0]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					timeZoneGap = -3;
				}
				lasthourForSendgird = Utility.lasthour_evening - (timeZoneGap);
				if (lasthourForSendgird > 24)
					lasthourForSendgird = lasthourForSendgird - 24;

			} else {

				try {
					timeZoneGap = 5 - Integer.parseInt(dataModel.zip_timezone.split("-")[1].split(":")[0]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					timeZoneGap = -3;
				}
				lasthourForSendgird = Utility.lasthour_evening + (timeZoneGap);
				if (lasthourForSendgird > 24)
					lasthourForSendgird = lasthourForSendgird - 24;

			}

		} else {
			lasthourForSendgird = Utility.lasthour_evening;
		}
		dataModel.scheduleSendforWhiteLabel = getScheduleSendForMailgun(Utility.lasthour_evening + ":" + Utility.lastMin_evening + ":" + Utility.lastSeconds_evening, dataModel.getZip_timezone());
		dataModel.open_at = getOpentimeForSendGrid(lasthourForSendgird + ":" + Utility.lastMin_evening + ":" + Utility.lastSeconds_evening, dataModel.getZip_timezone());

		return dataModel;

	}

	public CloudSearchModel getModelWithScheduleSendFor_Morning(CloudSearchModel dataModel) {

		int lastHour = 0, timeZoneGap = 0;
		// for evening scheduel send of 4 pm to 7 pm
		if (!dataModel.zip_timezone.contains("GMT-05") && !dataModel.zip_timezone.contains("GMT-5")) {
			String operator = "";

			if (dataModel.zip_timezone.contains("-")) {
				try {
					timeZoneGap = 5 - Integer.parseInt(dataModel.zip_timezone.split("-")[1].split(":")[0]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					timeZoneGap = -3;
				}
				lastHour = Utility.lasthour_morning - (timeZoneGap);
				if (lastHour > 24)
					lastHour = lastHour - 24;

			} else {

				try {
					timeZoneGap = 5 - Integer.parseInt(dataModel.zip_timezone.split("-")[1].split(":")[0]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					timeZoneGap = -3;
				}
				lastHour = Utility.lasthour_morning + (timeZoneGap);
				if (lastHour > 24)
					lastHour = lastHour - 24;

			}

		} else {
			lastHour = Utility.lasthour_morning;
		}
		dataModel.scheduleSendforWhiteLabel = getScheduleSendForMailgun(Utility.lasthour_morning + ":" + Utility.lastMin_morning + ":" + Utility.lastSeconds_morning, dataModel.getZip_timezone());
		dataModel.open_at = getOpentimeForSendGrid(lastHour + ":" + Utility.lastMin_morning + ":" + Utility.lastSeconds_morning, dataModel.getZip_timezone());

		return dataModel;

	}

	public static void setHandpicked_Subids(CloudSearchModel dataModel) {

		// we are setting the t2 and juju channel value for the handpicked
		if (!dataModel.providerFromQueue.equalsIgnoreCase("") && dataModel.providerFromQueue.toLowerCase().contains("handpicked")) {

			if (dataModel.providerFromQueue.toLowerCase().contains("bowering")) {
				dataModel.j2cT2Value = "&t2=bow";
				dataModel.jujuChannelValue = "bow";
			}

			else if (dataModel.providerFromQueue.toLowerCase().contains("elite")) {

				dataModel.j2cT2Value = "&t2=ejcs";
				dataModel.jujuChannelValue = "ejcs";
			}

			else if (dataModel.providerFromQueue.toLowerCase().contains("jobSerious")) {

				dataModel.j2cT2Value = "&t2=hpj";
				dataModel.jujuChannelValue = "hpj";
			}

			else if (dataModel.providerFromQueue.toLowerCase().contains("jobstomyinbox")) {

				dataModel.j2cT2Value = "&t2=jtmi";
				dataModel.jujuChannelValue = "jtmi";
			}

			else if (dataModel.providerFromQueue.toLowerCase().contains("miat")) {

				dataModel.j2cT2Value = "&t2=hpj";
				dataModel.jujuChannelValue = "hpj";
			}

			else if (dataModel.providerFromQueue.toLowerCase().contains("propath")) {

				dataModel.j2cT2Value = "&t2=hppp";
				dataModel.jujuChannelValue = "hppp";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("resume")) {
				dataModel.j2cT2Value = "&t2=RR";
				dataModel.jujuChannelValue = "RR";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("torch")) {
				dataModel.j2cT2Value = "&t2=tabf";
				dataModel.jujuChannelValue = "tabf";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("livecareer")) {
				dataModel.j2cT2Value = "&t2=lcja";
				dataModel.jujuChannelValue = "lcja";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("kgrosse")) {
				dataModel.j2cT2Value = "&t2=rec";
				dataModel.jujuChannelValue = "rec";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("bestpractice")) {
				dataModel.j2cT2Value = "&t2=bpa";
				dataModel.jujuChannelValue = "bpa";
			} else if (dataModel.providerFromQueue.toLowerCase().contains("medhealth")) {
				dataModel.j2cT2Value = "&t2=rec";
				dataModel.jujuChannelValue = "rec";
			}

			else {
				dataModel.j2cT2Value = "&t2=hpj";
				dataModel.jujuChannelValue = "hpj";
			}

		}

	}

	public String checkAscii_charater(String data) {
		for (int i = 0; i < data.length(); i++) {

			if (((int) data.charAt(i)) < 32 || ((int) data.charAt(i)) > 125) {
				data = data.replace("" + data.charAt(i), "");
			}
		}

		return data + "\n";
	}

	public void creating_whitelable_domain_advertisement_wise_stats_model(CloudSearchModel dataModel, QueueModelObject queue) {
		// here we are createing stats for users we processing from whilaeble
		// and domain wise
		String advertisement = "";
		DomainWiseSeekersCountModel model = Utility.whitelable_and_its_Domain_with_advertisement_stats.get(dataModel.getWhite_label());
		if (model == null) {

			model = new DomainWiseSeekersCountModel();
		}
		model.setWhitelabelName(dataModel.getWhite_label());
		model.getDomainsList().add(dataModel.getDomain_name());

		if (dataModel.getSendGridCategory().toLowerCase().contains("sparkpost")) {
			model.getDomain_email_client().put(dataModel.getDomain_name(), "sparkpost");
		} else if (!dataModel.getSendGridCategory().equalsIgnoreCase("")) {
			model.getDomain_email_client().put(dataModel.getDomain_name(), "sendgrid");
		} else {
			model.getDomain_email_client().put(dataModel.getDomain_name(), "mailgun");

		}
		HashMap<String, Integer> domain_with_advertisement_map = model.getMap_with_count().get(dataModel.getDomain_name());

		if (dataModel.getAdvertiseMent().equalsIgnoreCase("")) {
			if (dataModel.getProvider().equalsIgnoreCase("")) {
				advertisement = model.getWhitelabelName();
			} else {
				advertisement = dataModel.getProvider();
			}

		} else if (dataModel.getWhite_label().equalsIgnoreCase("jobstomyinbox")) {
			advertisement = "jobstomyinbox";
		} else {
			advertisement = dataModel.getAdvertiseMent();
		}

		if (domain_with_advertisement_map == null) {
			domain_with_advertisement_map = new HashMap<String, Integer>();
		}

		if (queue.getProcessType().toLowerCase().contains("web")) {
			advertisement = "web";
		}

		Integer lastCount = domain_with_advertisement_map.get(advertisement);

		if (lastCount == null) {
			lastCount = 1;
		} else {
			lastCount = lastCount + 1;
		}
		// putting last count in map
		domain_with_advertisement_map.put(advertisement, lastCount);
		// putting last map in model
		model.getMap_with_count().put(dataModel.getDomain_name(), domain_with_advertisement_map);
		Utility.whitelable_and_its_Domain_with_advertisement_stats.put(dataModel.getWhite_label(), model);
	}
}
