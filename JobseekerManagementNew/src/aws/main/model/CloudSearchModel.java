package aws.main.model;

/**
 * 
 * @author signity This is main model class of cloud search user
 */
public class CloudSearchModel {
	public String id = "";
	public String keyword = "", email = "", zipcode = "", latitude = "", longitude = "", alert_id = "";
	public double lowerLatitude = 0;
	public double lowerLongitude = 0;
	public double upperLongitude = 0;
	public double upperLatitude = 0;
	public String city = "";
	public String state = "";
	public String advertiseMent = "";
	public String registrationDate = "";
	public String click = "";
	public String open_at = "";
	public String provider_webAlerts = "";
	public String firstName = "";
	public String lastName = "";
	public String affiliate_code = "";
	public double radius = 0;
	public String frequency = "";
	public String providerFromResultSet = "";
	public String searchKeyword = "";
	public String scheduleSendforWhiteLabel = "";

	// public String postalAddress = "";

	public String userType = "";
	public String providerFromQueue = "";
	public String zip_timezone = "";
	public String main_provider = "";
	public String country = "";
	public String white_label = "";

	public String getWhite_label() {
		return white_label;
	}

	public void setWhite_label(String white_label) {
		this.white_label = white_label;
	}

	public String getMain_provider() {
		return main_provider;
	}

	public void setMain_provider(String main_provider) {
		this.main_provider = main_provider;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String sendGridAndMailGunMappingKey = "";

	public String getSendGridAndMailGunMappingKey() {
		return sendGridAndMailGunMappingKey;
	}

	public void setSendGridAndMailGunMappingKey(String sendGridAndMailGunMappingKey) {
		this.sendGridAndMailGunMappingKey = sendGridAndMailGunMappingKey;
	}

	public String getProviderFromResultSet() {
		return providerFromResultSet;
	}

	public void setProviderFromResultSet(String providerFromResultSet) {
		this.providerFromResultSet = providerFromResultSet;
	}

	public String getZip_timezone() {
		return zip_timezone;
	}

	public void setZip_timezone(String zip_timezone) {
		this.zip_timezone = zip_timezone;
	}

	public String getProviderFromQueue() {
		return providerFromQueue;
	}

	public void setProviderFromQueue(String providerFromQueue) {
		this.providerFromQueue = providerFromQueue;
	}

	public String j2cT2Value = "";
	public String jujuChannelValue = "";
	public String liveRampPixelValue = "";
	public String campaign_id = "";
	public String domain_name = "";
	public String dashboradFilename = "";
	public String pixelCategoryName = "";
	public String sendGridIpPool = "";
	public String sendGridCategory = "";

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getJ2cT2Value() {
		return j2cT2Value;
	}

	public void setJ2cT2Value(String j2cT2Value) {
		this.j2cT2Value = j2cT2Value;
	}

	public String getJujuChannelValue() {
		return jujuChannelValue;
	}

	public void setJujuChannelValue(String jujuChannelValue) {
		this.jujuChannelValue = jujuChannelValue;
	}

	public String getLiveRampPixelValue() {
		return liveRampPixelValue;
	}

	public void setLiveRampPixelValue(String liveRampPixelValue) {
		this.liveRampPixelValue = liveRampPixelValue;
	}

	public String getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}

	public String getDomain_name() {
		return domain_name;
	}

	public void setDomain_name(String domain_name) {
		this.domain_name = domain_name;
	}

	public String getDashboradFilename() {
		return dashboradFilename;
	}

	public void setDashboradFilename(String dashboradFilename) {
		this.dashboradFilename = dashboradFilename;
	}

	public String getPixelCategoryName() {
		return pixelCategoryName;
	}

	public void setPixelCategoryName(String pixelCategoryName) {
		this.pixelCategoryName = pixelCategoryName;
	}

	public String getSendGridIpPool() {
		return sendGridIpPool;
	}

	public void setSendGridIpPool(String sendGridIpPool) {
		this.sendGridIpPool = sendGridIpPool;
	}

	public String getSendGridCategory() {
		return sendGridCategory;
	}

	public void setSendGridCategory(String sendGridCategory) {
		this.sendGridCategory = sendGridCategory;
	}

	public String getScheduleSendforWhiteLabel() {
		return scheduleSendforWhiteLabel;
	}

	public void setScheduleSendforWhiteLabel(String scheduleSendforWhiteLabel) {
		this.scheduleSendforWhiteLabel = scheduleSendforWhiteLabel;
	}

	public String isDistanceLessThan70Miles = "NO";

	public String getIsDistanceLessThan70Miles() {
		return isDistanceLessThan70Miles;
	}

	public void setIsDistanceLessThan70Miles(String isDistanceLessThan70Miles) {
		this.isDistanceLessThan70Miles = isDistanceLessThan70Miles;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

	public String getProvider() {
		return providerFromResultSet;
	}

	public void setProvider(String provider) {
		this.providerFromResultSet = provider;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAlert_id() {
		return alert_id;
	}

	public void setAlert_id(String alert_id) {
		this.alert_id = alert_id;
	}

	public double getLowerLatitude() {
		return lowerLatitude;
	}

	public void setLowerLatitude(double lowerLatitude) {
		this.lowerLatitude = lowerLatitude;
	}

	public double getLowerLongitude() {
		return lowerLongitude;
	}

	public void setLowerLongitude(double lowerLongitude) {
		this.lowerLongitude = lowerLongitude;
	}

	public double getUpperLongitude() {
		return upperLongitude;
	}

	public void setUpperLongitude(double upperLongitude) {
		this.upperLongitude = upperLongitude;
	}

	public double getUpperLatitude() {
		return upperLatitude;
	}

	public void setUpperLatitude(double upperLatitude) {
		this.upperLatitude = upperLatitude;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAdvertiseMent() {
		return advertiseMent;
	}

	public void setAdvertiseMent(String advertiseMent) {
		this.advertiseMent = advertiseMent;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getClick() {
		return click;
	}

	public void setClick(String click) {
		this.click = click;
	}

	public String getOpen_at() {
		return open_at;
	}

	public void setOpen_at(String open_at) {
		this.open_at = open_at;
	}

	public String getProvider_webAlerts() {
		return provider_webAlerts;
	}

	public void setProvider_webAlerts(String provider_webAlerts) {
		this.provider_webAlerts = provider_webAlerts;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAffiliate_code() {
		return affiliate_code;
	}

	public void setAffiliate_code(String affiliate_code) {
		this.affiliate_code = affiliate_code;
	}

	// never change this be care full this is used in the queueu and process
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.id + "|" + this.keyword + "|" + this.zipcode + "|" + this.email + "|" + this.advertiseMent + "|" + this.registrationDate + "|" + this.city + "|" + this.state + "|" + this.latitude + "|" + this.longitude + "|" + this.upperLatitude + "|" + this.upperLongitude + "|" + this.lowerLatitude + "|" + this.lowerLongitude + "|" + this.click + "|" + this.open_at + "|" + this.provider_webAlerts + "|" + this.firstName + "|" + this.lastName + "|" + this.affiliate_code + "|" + this.radius + "|" + this.providerFromResultSet + "|" + this.searchKeyword + "|" + this.isDistanceLessThan70Miles + "|" + this.scheduleSendforWhiteLabel + "|" + this.zip_timezone + "|" + this.dashboradFilename + "|" + this.userType + "|" + this.providerFromQueue + "|" + this.j2cT2Value + "|"
				+ this.jujuChannelValue + "|" + this.liveRampPixelValue + "|" + this.campaign_id + "|" + this.domain_name + "|" + this.pixelCategoryName + "|" + this.sendGridIpPool + "|" + this.sendGridCategory + "|" + this.country + "|" + this.sendGridAndMailGunMappingKey + "|" + this.main_provider + "|" + this.white_label + "\n";
	}
}
