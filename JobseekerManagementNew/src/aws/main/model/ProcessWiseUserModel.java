package aws.main.model;

import java.util.HashMap;
/**
 * 
 * @author signity
 *This class is for Processwise users stats keep in file
 */

public class ProcessWiseUserModel {

	private String processType;
	HashMap<String, Integer> processWiseStats = new HashMap<String, Integer>();

	public HashMap<String, Integer> getProcessWiseStats () {
		return processWiseStats;
	}

	public void setProcessWiseStats (HashMap<String, Integer> processWiseStats) {
		this.processWiseStats = processWiseStats;
	}

	public String getProcessType () {
		return processType;
	}

	public void setProcessType (String processType) {
		this.processType = processType;
	}

}
