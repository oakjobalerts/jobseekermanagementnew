package aws.main.model;
/**
 * 
 * 
 * @author signity
 *This is the main model class for Queue object.
 *In which we are setting required value for queue from its apis
 */
public class QueueModelObject {

	private String processName = "";
	private String processType = "";
	private String queueName = "";
	private String query = "";
	private String isActive = "";
	private String upTo = "";
	private String no_of_queue = "";
	private String no_of_days = "";
	private String selecteddays = "";
	private String queueUrl = "";
	public String from = "";
	// public String invalidDomainString = "";
	public String userType = "";
	public String providerFromQueue = "";
	public String j2cT2Value = "";
	public String jujuChannelValue = "";
	public String liveRampPixelValue = "";
	public String campaign_id = "";
	public String domain_name = "";
	// public String dashboradFilename = "";
	public String pixelCategoryName = "";
	public String sendGridIpPool = "";
	public String sendGridCategory = "";
	// this is for the sendgrid and mailgun mapping key which will help in click
	// loggin
	public String sendgridMappingKey = "";

	private String postalAddress = "";
	private String arborpixel = "";
	private String white_label = "";
	private String image_postaladdress = "";

	private boolean isRampup = false;
	private String today_rampup_status = "";


	private String ramup_type = "";

	public String getToday_rampup_status() {
		return today_rampup_status;
	}

	public void setToday_rampup_status(String today_rampup_status) {
		this.today_rampup_status = today_rampup_status;
	}

	private String id = "";

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isRampup() {
		return isRampup;
	}

	public void setRampup(boolean isRampup) {
		this.isRampup = isRampup;
	}

	public String getRamup_type() {
		return ramup_type;
	}

	public void setRamup_type(String ramup_type) {
		this.ramup_type = ramup_type;
	}

	public double getTotalRecordInDb() {
		return totalRecordInDb;
	}

	public void setTotalRecordInDb(double totalRecordInDb) {
		this.totalRecordInDb = totalRecordInDb;
	}

	public String getImage_postaladdress() {
		return image_postaladdress;
	}

	public void setImage_postaladdress(String image_postaladdress) {
		this.image_postaladdress = image_postaladdress;
	}

	public double totalRecordInDb = 0;

	public String getWhite_label() {
		return white_label;
	}

	public void setWhite_label(String white_label) {
		this.white_label = white_label;
	}

	public String getArborpixel() {
		return arborpixel;
	}

	public void setArborpixel(String arborpixel) {
		this.arborpixel = arborpixel;
	}

	public String getSubjectFileName() {
		return subjectFileName;
	}

	public void setSubjectFileName(String subjectFileName) {
		this.subjectFileName = subjectFileName;
	}

	private String subjectFileName = "";

	public String getSendgridMappingKey() {
		return sendgridMappingKey;
	}

	public void setSendgridMappingKey(String sendgridMappingKey) {
		this.sendgridMappingKey = sendgridMappingKey;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getProviderFromQueue() {
		return providerFromQueue;
	}

	public void setProviderFromQueue(String providerFromQueue) {
		this.providerFromQueue = providerFromQueue;
	}

	public String getJ2cT2Value() {
		return j2cT2Value;
	}

	public void setJ2cT2Value(String j2cT2Value) {
		this.j2cT2Value = j2cT2Value;
	}

	public String getJujuChannelValue() {
		return jujuChannelValue;
	}

	public void setJujuChannelValue(String jujuChannelValue) {
		this.jujuChannelValue = jujuChannelValue;
	}

	public String getLiveRampPixelValue() {
		return liveRampPixelValue;
	}

	public void setLiveRampPixelValue(String liveRampPixelValue) {
		this.liveRampPixelValue = liveRampPixelValue;
	}

	public String getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(String campaign_id) {
		this.campaign_id = campaign_id;
	}

	public String getDomain_name() {
		return domain_name;
	}

	public void setDomain_name(String domain_name) {
		this.domain_name = domain_name;
	}

	// public String getDashboradFilename () {
	// return dashboradFilename;
	// }
	//
	// public void setDashboradFilename (String dashboradFilename) {
	// this.dashboradFilename = dashboradFilename;
	// }

	public String getPixelCategoryName() {
		return pixelCategoryName;
	}

	public void setPixelCategoryName(String pixelCategoryName) {
		this.pixelCategoryName = pixelCategoryName;
	}

	public String getSendGridIpPool() {
		return sendGridIpPool;
	}

	public void setSendGridIpPool(String sendGridIpPool) {
		this.sendGridIpPool = sendGridIpPool;
	}

	public String getSendGridCategory() {
		return sendGridCategory;
	}

	public void setSendGridCategory(String sendGridCategory) {
		this.sendGridCategory = sendGridCategory;
	}

	// public String getInvalidDomainString () {
	// return invalidDomainString;
	// }
	//
	// public void setInvalidDomainString (String invalidDomainString) {
	// this.invalidDomainString = invalidDomainString;
	// }

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getQueueUrl() {
		return queueUrl;
	}

	public void setQueueUrl(String queueUrl) {
		this.queueUrl = queueUrl;
	}

	private int uptoWhichUser = 0;

	public int getUptoWhichUser() {
		return uptoWhichUser;
	}

	public void setUptoWhichUser(int uptoWhichUser) {
		this.uptoWhichUser = uptoWhichUser;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getUpTo() {
		return upTo;
	}

	public void setUpTo(String upTo) {
		this.upTo = upTo;
	}

	public String getNo_of_queue() {
		return no_of_queue;
	}

	public void setNo_of_queue(String no_of_queue) {
		this.no_of_queue = no_of_queue;
	}

	public String getNo_of_days() {
		return no_of_days;
	}

	public void setNo_of_days(String no_of_days) {
		this.no_of_days = no_of_days;
	}

	public String getSelecteddays() {
		return selecteddays;
	}

	public void setSelecteddays(String selecteddays) {
		this.selecteddays = selecteddays;
	}

}
