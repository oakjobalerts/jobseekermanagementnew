package aws.main.model;

/**
 * 
 * @author signity This class is for Keyword and its corresponding searchkeyword
 */
public class MapKeyWordModel {

	public String keyword = "";
	public String searchKeyword = "";

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getSearchKeyword() {
		return searchKeyword;
	}

	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}

}
