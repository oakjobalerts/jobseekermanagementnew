package aws.main.model;
/**
 * 
 * 
 * @author signity
 *This is Main class for Rampup model .
 *
 *It handle all parameter need to set rampup
 */
public class RampUpModel {

	private String processName = "";
	private String from_table_name = "";
	private String to_table_name = "";
	private String ramp_users_count = "";
	private String ramp_up_day_count = "";
	private String date = "";
	private String ramp_up_type = "";
	private String rampup_advertisement = "";
	private boolean is_advertise_use = false;
	private String is_rampup_live = "0";
	private boolean today_rampup_status = false;
	private String type_of_users = "ALL";
	private String is_move_or_copy = "C";
	private String provider = "";
	private boolean is_priority_use = false;
	private Integer rampup_priority = 0;
	private boolean isDatecondition_use=false;
	private String dayCount="";
	private String user_domain="";
	private boolean is_new_import=true;

	private String domain_type="";

	public String getDomain_type() {
		return domain_type;
	}

	public void setDomain_type(String domain_type) {
		this.domain_type = domain_type;
	}

	public boolean isIs_new_import() {
		return is_new_import;
	}

	public void setIs_new_import(boolean is_new_import) {
		this.is_new_import = is_new_import;
	}

	public String getUser_domain () {
		return user_domain;
	}

	public void setUser_domain (String user_domain) {
		this.user_domain = user_domain;
	}

	public boolean isDatecondition_use () {
		return isDatecondition_use;
	}

	public void setDatecondition_use (boolean isDatecondition_use) {
		this.isDatecondition_use = isDatecondition_use;
	}

	public String getDayCount () {
		return dayCount;
	}

	public void setDayCount (String dayCount) {
		this.dayCount = dayCount;
	}

	public boolean isIs_priority_use () {
		return is_priority_use;
	}

	public void setIs_priority_use (boolean is_priority_use) {
		this.is_priority_use = is_priority_use;
	}

	public Integer getRampup_priority () {
		return rampup_priority;
	}

	public void setRampup_priority (Integer rampup_priority) {
		this.rampup_priority = rampup_priority;
	}

	
	
	private String starting_index = "0";
	private Integer percent = 0;
	

	public Integer getPercent () {
		return percent;
	}

	public void setPercent (Integer percent) {
		this.percent = percent;
	}

	public String getStarting_index () {
		return starting_index;
	}

	public void setStarting_index (String starting_index) {
		this.starting_index = starting_index;
	}

	public String getProvider () {
		return provider;
	}

	public void setProvider (String provider) {
		this.provider = provider;
	}

	public String getType_of_users () {
		return type_of_users;
	}

	public void setType_of_users (String type_of_users) {
		this.type_of_users = type_of_users;
	}

	public String getIs_move_or_copy () {
		return is_move_or_copy;
	}

	public void setIs_move_or_copy (String is_move_or_copy) {
		this.is_move_or_copy = is_move_or_copy;
	}

	public boolean isToday_rampup_status () {
		return today_rampup_status;
	}

	public void setToday_rampup_status (boolean today_rampup_status) {
		this.today_rampup_status = today_rampup_status;
	}

	public String getProcessName () {
		return processName;
	}

	public void setProcessName (String processName) {
		this.processName = processName;
	}

	public String getFrom_table_name () {
		return from_table_name;
	}

	public void setFrom_table_name (String from_table_name) {
		this.from_table_name = from_table_name;
	}

	public String getTo_table_name () {
		return to_table_name;
	}

	public void setTo_table_name (String to_table_name) {
		this.to_table_name = to_table_name;
	}

	public String getRamp_users_count () {
		return ramp_users_count;
	}

	public void setRamp_users_count (String ramp_users_count) {
		this.ramp_users_count = ramp_users_count;
	}

	public String getRamp_up_day_count () {
		return ramp_up_day_count;
	}

	public void setRamp_up_day_count (String ramp_up_day_count) {
		this.ramp_up_day_count = ramp_up_day_count;
	}

	public String getDate () {
		return date;
	}

	public void setDate (String date) {
		this.date = date;
	}

	public String getRamp_up_type () {
		return ramp_up_type;
	}

	public void setRamp_up_type (String ramp_up_type) {
		this.ramp_up_type = ramp_up_type;
	}

	public String getRampup_advertisement () {
		return rampup_advertisement;
	}

	public void setRampup_advertisement (String rampup_advertisement) {
		this.rampup_advertisement = rampup_advertisement;
	}

	public boolean isIs_advertise_use () {
		return is_advertise_use;
	}

	public void setIs_advertise_use (boolean is_advertise_use) {
		this.is_advertise_use = is_advertise_use;
	}

	public String getIs_rampup_live () {
		return is_rampup_live;
	}

	public void setIs_rampup_live (String is_rampup_live) {
		this.is_rampup_live = is_rampup_live;
	}

}
