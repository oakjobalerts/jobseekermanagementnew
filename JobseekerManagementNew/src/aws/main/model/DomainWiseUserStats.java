package aws.main.model;

import java.util.HashMap;

/**
 * 
 * @author signity This class is bascally used to Domainwise users processed
 *         stats
 */
public class DomainWiseUserStats {

	private String procesSName;
	private String gmailCount;
	private String yahooCount;
	private String aolCount;
	private String othersCount;

	public String getGmailCount() {
		return gmailCount;
	}

	public void setGmailCount(String gmailCount) {
		this.gmailCount = gmailCount;
	}

	public String getYahooCount() {
		return yahooCount;
	}

	public void setYahooCount(String yahooCount) {
		this.yahooCount = yahooCount;
	}

	public String getAolCount() {
		return aolCount;
	}

	public void setAolCount(String aolCount) {
		this.aolCount = aolCount;
	}

	public String getOthersCount() {
		return othersCount;
	}

	public void setOthersCount(String othersCount) {
		this.othersCount = othersCount;
	}

	public String getProcesSName() {
		return procesSName;
	}

	public void setProcesSName(String procesSName) {
		this.procesSName = procesSName;
	}

}
