package aws.main.model;

import java.util.HashMap;

/**
 * 
 * 
 * @author signity
 *  This is class for Invalid domain Model
 */

public class InvalidDomainModel {

	private HashMap<String, Integer> invalidMap = new HashMap<String, Integer>();

	public HashMap<String, Integer> getInvalidMap() {
		return invalidMap;
	}

	public void setInvalidMap(HashMap<String, Integer> invalidMap) {
		this.invalidMap = invalidMap;
	}

}
