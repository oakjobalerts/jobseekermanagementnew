package aws.main.model;

/**
 * 
 * @author signity This model is basically used for processing Multiple
 *         whitelables from one process
 */
public class CombineProcessModel {

	public String domainName = "";
	public String category_or_compaingIds = "";
	public int totalNumberOfUsers = 0;
	private String userProvider = "";
	private String global_campaing_sendgrid = "";
	private String arborpixel = "";
	private String white_label = "";
	private String image_postaladdress = "";

	public String getImage_postaladdress() {
		return image_postaladdress;
	}

	public void setImage_postaladdress(String image_postaladdress) {
		this.image_postaladdress = image_postaladdress;
	}

	public String getWhite_label() {
		return white_label;
	}

	public void setWhite_label(String white_label) {
		this.white_label = white_label;
	}

	public String getArborpixel() {
		return arborpixel;
	}

	public void setArborpixel(String arborpixel) {
		this.arborpixel = arborpixel;
	}

	private String postalAddress = "";

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	private String subjectFileName = "";

	public String getSubjectFileName() {
		return subjectFileName;
	}

	public void setSubjectFileName(String subjectFileName) {
		this.subjectFileName = subjectFileName;
	}

	public String getGlobal_campaing_sendgrid() {
		return global_campaing_sendgrid;
	}

	public void setGlobal_campaing_sendgrid(String global_campaing_sendgrid) {
		this.global_campaing_sendgrid = global_campaing_sendgrid;
	}

	public String getUserProvider() {
		return userProvider;
	}

	public void setUserProvider(String userProvider) {
		this.userProvider = userProvider;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getCategory_or_compaingIds() {
		return category_or_compaingIds;
	}

	public void setCategory_or_compaingIds(String category_or_compaingIds) {
		this.category_or_compaingIds = category_or_compaingIds;
	}

	public int getTotalNumberOfUsers() {
		return totalNumberOfUsers;
	}

	public void setTotalNumberOfUsers(int totalNumberOfUsers) {
		this.totalNumberOfUsers = totalNumberOfUsers;
	}

}
