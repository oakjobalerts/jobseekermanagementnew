package aws.main.model;

/**
 * 
 * @author signity
 *This is main class of Queue object 
 *It get directly converted into models
 */
public class ModelObjectQueue {

	public String isActive = "";
	public String processName = "";
	public String processType = "";
	public String countQuery = "";
	public String dataQuery = "";
	public String numberOfWeekDays="";

	public String getNumberOfWeekDays() {
		return numberOfWeekDays;
	}

	public void setNumberOfWeekDays(String numberOfWeekDays) {
		this.numberOfWeekDays = numberOfWeekDays;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getProcessName() {
		return processName;
	}

	public void setProcessName(String processName) {
		this.processName = processName;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getCountQuery() {
		return countQuery;
	}

	public void setCountQuery(String countQuery) {
		this.countQuery = countQuery;
	}

	public String getDataQuery() {
		return dataQuery;
	}

	public void setDataQuery(String dataQuery) {
		this.dataQuery = dataQuery;
	}

}
