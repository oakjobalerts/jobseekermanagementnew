package aws.main.utility;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.ws.rs.core.MediaType;

import net.spy.memcached.MemcachedClient;

import org.json.JSONArray;
import org.json.JSONObject;

import aws.main.model.CombineProcessModel;
import aws.main.model.DomainWiseSeekersCountModel;
import aws.main.model.DomainWiseUserStats;
import aws.main.model.InvalidDomainModel;
import aws.main.model.MapKeyWordModel;
import aws.main.model.ProcessWiseUserModel;
import aws.main.model.QueueModelObject;
import aws.main.model.RampUpModel;
import aws.main.service.MainClass;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.PurgeQueueRequest;
import com.csvreader.CsvWriter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * 
 * 
 * 
 * @author Pawan
 * 
 *         This is class for all the utility of this project like database
 *         connection file read and write process
 * 
 *         {@link #getContent(String) #getInvalid_Domain_keyword()
 *         #initialize_domain_campaign_wise_map(String)
 *         #initialize_domain_wise_statst_map(String)
 *         #initialize_invalid_domain_wise_statst_map(String)
 *         #iniTialize_map_for_421_users()
 *         #initialize_process_wise_statst_map(String)
 *         #initialize_set_for_providers_name(String)
 *         #initialize_set_for_providers_name(String)
 *         #initializeChannelWithProvider()
 *         #initializeMemcacheFor_Oak_TheKeyword()
 *         #initializesearch_keyword_for_oakjob_newApi() *}
 * 
 *
 */
public class Utility {

	/**
	 * 
	 * These are all variable we are using in this class
	 * 
	 * 
	 */
	public static final String duplicateUserKey = "duplicateusers";

	public static AWSCredentials credentials = null;
	public static AmazonSQS sqs;

	// ==============================================================================================
	// credential for json account on cloudsearch......................
	public static String awsAccessKey = "AKIAI3XCS2J6KODBLXVQ";
	public static String awsSecretKey = "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";

	public static SimpleDateFormat scheduleSendDateFormatFOrBoth_sendgrid_Mailgun = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");

	public static SimpleDateFormat gmtScheduleSendDateFormat = new SimpleDateFormat("dd-M-yyyy HH:mm:ss");

	// for paper rosen=======================//=====================
	// public static String awsAccessKey_paperRosen = "AKIAJB4W4NEOUTCHYQQA";
	// public static String awsSecretKey_paperRosen =
	// "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";

	public static String searchEndpoint = "search-jobs-r62mfdzldhpjsmvehw4yuasvvi.us-west-2.cloudsearch.amazonaws.com";
	public static String documentEndpoint = "doc-jobs-r62mfdzldhpjsmvehw4yuasvvi.us-west-2.cloudsearch.amazonaws.com";
	public static int dataCount = 0;
	// ==============================================================================================
	// for json database..............
	// static String HOST_NAME = "172.31.55.113";// FOR EAST REGION
	// static String username = "root";
	// static String password = "qazwsx@123";
	// //
	// ==============================================================================================
	// static String DB_NAME = "oakalerts";

	// credential for the rds data base..........................

	public static String HOST_NAME = "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com:3306";//
	// FOR
	/**
	 * Follwing are the database variables details
	 */
	public static String DB_NAME = "oakalerts";
	public static String username = "awsoakuser";
	public static String password = "awsoakusersignity";
	public static Connection con = null;
	public static String url = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;

	// for rds data base................................................

	// static String HOST_NAME =fromIndex
	// "oakuserdbinstance-cluster.cluster-ca2bixk3jmwi.us-east-1.rds.amazonaws.com";
	// static String username = "awsoakuser";
	// static String password = "awsoakusersignity";
	// ==============================================================================================
	/**
	 * These are the all variable we are using in this class and project
	 * 
	 */
	public static String forWhichQueueIsMade = "";
	public static int totalMessage = 0;
	public static int threadName = 0;
	public static int numberOfThread = 0;
	public static int threadCompled = 0;
	public static String lempop = "";
	// public static String queueName = "";
	public static int usersPerMessage = 40;
	public static String dbUsersCountQuery = "";
	public static String dbDataQuery = "";
	public static String upsUpdateQuery = "";
	public static MemcachedClient memcacheObj;
	public static AtomicInteger userProcessingCount = new AtomicInteger(0);
	public static AtomicInteger totalUserFromLoop = new AtomicInteger(0);
	public static int totalUsersInDb = 0;
	public static AtomicInteger usersNotProcessed = new AtomicInteger(0);
	public static int duplicateUsers;
	public static int invalidUsers;
	public static int searchKeyword_found = 0;
	public static int invalid_domain_block_users = 0;
	public static int pipeContainingKeywordsUsers;
	public static int nurseWiseKeywordsUsersCount;
	public static int uk_elite_users;
	public static int users_with_421_error;
	public static String invalidDomainString = "";
	public static boolean isForOakjobProcess = false;
	public static SimpleDateFormat sendgrid_dateformat = new SimpleDateFormat("dd-M-yyyy HH:mm:ss'Z'");
	public static String sendgrid_currdate = "";
	public static int totalList = 0;
	public static String finalMailText = "";
	public static int numberOfActiveQueueCount = 0;
	public static AtomicInteger totalNumberOfQueueus = new AtomicInteger(0);
	public static String domainWiseUserStatsFile = "/var/nfs-93/redirect/mis_logs/DomainWiseUsers";
	public static String windward_rampup = "/var/nfs-93/redirect/mis_logs/queuemaking/windward_rampup.txt";
	public static String s = "/var/nfs-93/redirect/mis_logs/queuemaking/torch_gmail_rampup.txt";
	public static String torch_rampupFile = "/var/nfs-93/redirect/mis_logs/queuemaking/torch_rampup.txt";
	public static String jobkernal_rampup = "/var/nfs-93/redirect/mis_logs/queuemaking/jobkernal-rampup.txt";
	public static String linkus_rampup = "/var/nfs-93/redirect/mis_logs/queuemaking/linkus_rampup.txt";
	public static String tierone_rampup = "/var/nfs-93/redirect/mis_logs/queuemaking/tierone_rampup.txt";
	public static String dimond_rampup = "/var/nfs-93/redirect/mis_logs/queuemaking/deck_in_diamond_rampup.txt";
	public static String baseFilePath = "/var/nfs-93/redirect/mis_logs/";
	public static String filePath = "/var/nfs-93/redirect/";
	public static String mis_filePath = "/var/nfs-93/redirect/mis_logs";
	public static int lasthour_morning, lastMin_morning = 0, lastSeconds_morning = 0;
	public static int secondsIntervalIn_schedule = 30;
	// gmt time is 11 to 16 and it will act as 7 am in est to 12 est
	public static int startHour_morning = 12;
	public static int uptoHourInterval_morning = 17;
	public static Region queueRegion = Region.getRegion(Regions.US_EAST_1);
	static String completeProcessUrl = "http://webservices.elasticbeanstalk.com/jar/processcompleted/?processId=";
	static String isUser_1to7Or_8to14 = "";
	public static int lasthour_evening, lastMin_evening = 0, lastSeconds_evening = 0;
	public static int startHour_evening = 20;
	public static int uptoHourInterval_evening = 23;

	/**
	 * 
	 * These are the all collection classes we are using in this project
	 * 
	 */
	public static ArrayList<String> inValidDomainList = new ArrayList<String>();
	static ArrayList<String> testingMails = new ArrayList<String>();
	public static ArrayList<String> inValidDomain_blcok_keyword_list = new ArrayList<String>();

	public static SimpleDateFormat jobkernalDateformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat oakjobOpen_dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static HashMap<String, ArrayList<MapKeyWordModel>> oak_white_label_keyword_mapping_map = new HashMap<String, ArrayList<MapKeyWordModel>>();
	public static HashMap<String, ArrayList<MapKeyWordModel>> oak_job_keyword_mapping_map = new HashMap<String, ArrayList<MapKeyWordModel>>();
	public static HashMap<String, Integer> searchKeywordAndLengthCount = new HashMap<String, Integer>();
	public static HashMap<String, ArrayList<MapKeyWordModel>> local_keyword_mapping_map = new HashMap<String, ArrayList<MapKeyWordModel>>();
	public static HashMap<String, Integer> domainWiseUsersStats = new HashMap<String, Integer>();
	public static HashMap<String, DomainWiseUserStats> preExistingHashMap_DomainWiseHashMap = new HashMap<String, DomainWiseUserStats>();
	public static HashMap<String, InvalidDomainModel> invalidDomain_users_count_map = new HashMap<String, InvalidDomainModel>();
	public static HashMap<String, Boolean> map_with_421_error_users = new HashMap<String, Boolean>();
	public static List<QueueModelObject> queuesArrayList = new ArrayList<QueueModelObject>();
	public static HashMap<String, ProcessWiseUserModel> processWiseUserstats_map = new HashMap<String, ProcessWiseUserModel>();
	public static HashMap<String, ProcessWiseUserModel> preExistingHashMap_ProcessWise_HashMap = new HashMap<String, ProcessWiseUserModel>();
	public static HashMap<String, String> domain_campaign_status_map = new HashMap<String, String>();
	public static Set<String> provider_Sets = new HashSet<String>();
	public static HashMap<String, ArrayList<CombineProcessModel>> queueCountHashMap_CombineProcess = new HashMap<String, ArrayList<CombineProcessModel>>();
	public static HashMap<String, Integer> queue_AndTheir_count_hashMap = new HashMap<String, Integer>();
	public static ArrayList<RampUpModel> rampUpModelist = new ArrayList<RampUpModel>();
	public static HashMap<String, ArrayList<MapKeyWordModel>> college_keyword_mapping_map = new HashMap<String, ArrayList<MapKeyWordModel>>();

	public static HashMap<String, DomainWiseSeekersCountModel> whitelable_and_its_Domain_with_advertisement_stats = new HashMap<String, DomainWiseSeekersCountModel>();

	/**
	 * 
	 * 
	 * @param url
	 * @return String of all the queries we need to processed
	 * @throws Exception
	 */
	// ==============================================================================================
	public static String getContent(String url) throws Exception {
		URL website = new URL(url);
		URLConnection connection = website.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

		StringBuilder response = new StringBuilder();
		String inputLine;

		while ((inputLine = in.readLine()) != null)
			response.append(inputLine);

		in.close();

		return response.toString();
	}

	// ==============================================================================================
	/**
	 * 
	 * 
	 * @param subject
	 * @param size
	 * @param dbSize
	 * @param duplicateUser
	 * @param userNotProcessed
	 * @param Countquery
	 * @param dataQuery
	 * @param upsUpdateQuery
	 * @return String if email is sending properly
	 */
	public static String sendEmail(String subject, int size, int dbSize, int duplicateUser, int userNotProcessed, String Countquery, String dataQuery, String upsUpdateQuery) {
		String[] TestBcclist = new String[5];
		TestBcclist[0] = "gagan@mailtesting.us";
		TestBcclist[1] = "mangesh@mailtesting.us";
		TestBcclist[2] = "gagan.gill@signitysolutions.com";
		TestBcclist[3] = "parveen.k@signitysolutions.in";
		TestBcclist[4] = "gurpreet.s@signitysolutions.in";

		SendGrid sendgrid;
		SendGrid.Email email;
		// create global sendgrid obj
		sendgrid = new SendGrid("oakjobscom", "fbg3002056-3");
		email = new SendGrid.Email();
		// sending mail from email id..
		email.setFrom("alerts@oakjobs.com");
		email.setSubject(subject);
		email.setHtml("<html><body>Total number of users in queue are = " + size + " <br>Total number of users in Database= " + dbSize + "<br>" + "ToTal number of Duplicate user are=" + duplicateUser + " <br>Total number of UserNotProcessd= " + userNotProcessed + " <br><br><b>Query for the Data base count=</b>" + Countquery + " <br><br><b> Query for the Data=</b>" + dataQuery + " <br><br><b> Query for ups Data=</b>" + upsUpdateQuery + " </body></html>");
		email.addTo("pawan@signitysolutions.co.in");
		email.setBcc(TestBcclist);
		try {
			SendGrid.Response response = sendgrid.send(email);
			if (response.getStatus()) {
				return "success";
			}
		} catch (SendGridException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "error";
	}

	// ==============================================================================================
	/**
	 * 
	 * 
	 * @param htmlBody
	 * @param queueCountHashMap_CombineProcess
	 */
	public static void sendEmail(String htmlBody, HashMap<String, ArrayList<CombineProcessModel>> queueCountHashMap_CombineProcess) {

		String[] TestBcclist = new String[6];
		TestBcclist[0] = "gagan@mailtesting.us";
		TestBcclist[1] = "mangesh@mailtesting.us";
		TestBcclist[2] = "gagan.gill@signitysolutions.com";
		TestBcclist[3] = "parveen.k@signitysolutions.in";
		TestBcclist[4] = "kanav@signitysolutions.com";
		TestBcclist[5] = "abhishek.k@signitysolutions.in";

		SendGrid sendgrid;
		SendGrid.Email email;
		// create global sendgrid obj
		sendgrid = new SendGrid("oakjobscom", "fbg3002056-3");
		email = new SendGrid.Email();
		// sending mail from email id..
		email.setFrom("alerts@oakjobs.com");
		String subject = "";

		if (queueCountHashMap_CombineProcess.size() == 1) {
			// "Today " + numberOfQueues + " has been created FROM DASHBOARD..."
			Iterator it = Utility.queueCountHashMap_CombineProcess.entrySet().iterator();
			while (it.hasNext()) {

				Map.Entry pair = (Map.Entry) it.next();
				subject = "Today " + pair.getKey() + " queue has been created";
			}
		} else {
			subject = "Today " + queueCountHashMap_CombineProcess.size() + " has been created FROM DASHBOARD...";

		}

		email.setSubject(subject);
		email.setHtml(htmlBody);
		email.addTo("pawan@signitysolutions.co.in");
		email.setBcc(TestBcclist);

		try {
			SendGrid.Response response = sendgrid.send(email);
			if (response.getStatus()) {
				System.out.println("send");
				// System.out.println("========================queues right now="
				// + Utility.totalNumberOfQueueus.get()
				// + "=========================================");
				// Utility.finalMailText = "";
				// Utility.totalNumberOfQueueus.set(0);

				// try {
				// Thread.sleep(2000);
				// }
				// catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// return true;
			}
		} catch (SendGridException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return false;

	}

	// =============================================//=======================================
	/**
	 * 
	 * @param body
	 *            we are sending email just for alert
	 */
	public static void sendEmailJustForAlerts(String body) {

		String[] TestBcclist = new String[4];
		TestBcclist[0] = "gagan@mailtesting.us";
		TestBcclist[1] = "gagan.gill@signitysolutions.com";
		TestBcclist[2] = "kanav@signitysolutions.com";
		TestBcclist[3] = "jaspreet@signitysolutions.in";

		SendGrid sendgrid;
		SendGrid.Email email;
		// create global sendgrid obj
		sendgrid = new SendGrid("oakjobscom", "fbg3002056-3");
		email = new SendGrid.Email();
		// sending mail from email id..
		email.setFrom("alerts@oakjobs.com");

		email.setSubject(body);

		email.setHtml("<html>" + body + "</html>");
		email.setBcc(TestBcclist);
		email.addTo("pawan@signitysolutions.co.in");
		try {
			SendGrid.Response response = sendgrid.send(email);
			if (response.getStatus()) {
				System.out.println("send");
			}
		} catch (SendGridException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// ==============================================================================================

	/**
	 * 
	 * 
	 * @param size
	 * @param modelObject
	 *            writing size of users in file
	 */

	public static synchronized void writeSizeIntoFile_forCombineProcess
	(int size, QueueModelObject modelObject) {
		File log_dashboard_folder = null;
		FileWriter fileWriterObj = null;

		ArrayList<CombineProcessModel> lastList = null;
		System.out.println("writing stats of in file for the child process");

		try {
			// lastCount =
			// Utility.queueCountHashMap.get(modelObject.getQueueName().trim());
			lastList = Utility.queueCountHashMap_CombineProcess.get(modelObject.getQueueName().trim());

			if (lastList == null) {
				// for global object
				// else if (lastList == null) {
				CombineProcessModel parentProcessModel = new CombineProcessModel();
				parentProcessModel.setCategory_or_compaingIds(modelObject.getQueueName().trim() + "_global");
				parentProcessModel.setDomainName(modelObject.getDomain_name());
				parentProcessModel.setTotalNumberOfUsers(size);
				parentProcessModel.setUserProvider(modelObject.getProviderFromQueue().trim());

				lastList = new ArrayList<CombineProcessModel>();
				// for the first child process
				CombineProcessModel childProcessModel = new CombineProcessModel();
				if (!modelObject.getSendGridCategory().equalsIgnoreCase("") && !modelObject.getSendGridCategory().contains("sparkpost")) {
					// setting globla and chilc sendgrid category
					childProcessModel.setCategory_or_compaingIds(modelObject.getSendGridCategory().trim());
					parentProcessModel.setGlobal_campaing_sendgrid(modelObject.getSendGridCategory().trim());

				} else {
					childProcessModel.setCategory_or_compaingIds(modelObject.getCampaign_id());
					parentProcessModel.setGlobal_campaing_sendgrid(modelObject.getCampaign_id());

				}
				childProcessModel.setDomainName(modelObject.getDomain_name());
				childProcessModel.setTotalNumberOfUsers(size);
				childProcessModel.setUserProvider(modelObject.getProviderFromQueue().trim());
				// new code
				childProcessModel.setSubjectFileName(modelObject.getSubjectFileName());
				childProcessModel.setPostalAddress(modelObject.getPostalAddress().replaceAll("\\|", " "));
				childProcessModel.setImage_postaladdress(modelObject.getImage_postaladdress().replaceAll("\\|", " "));
				childProcessModel.setArborpixel(modelObject.getArborpixel().replaceAll("\\|", ""));
				childProcessModel.setWhite_label(modelObject.getWhite_label().replaceAll("\\|", ""));

				// add two object fro the first time so that we can get the key
				// for gloabal as well as the first child
				lastList.add(parentProcessModel);
				lastList.add(childProcessModel);
				Utility.queueCountHashMap_CombineProcess.put(modelObject.getQueueName().trim(), lastList);
			} else {
				// for the others child
				lastList = Utility.queueCountHashMap_CombineProcess.get(modelObject.getQueueName().trim());

				for (int i = 0; i < lastList.size(); i++) {

					if (lastList.get(i).getCategory_or_compaingIds().toLowerCase().contains("global")) {

						int newGlobalSize = lastList.get(i).getTotalNumberOfUsers() + size;

						lastList.get(i).setTotalNumberOfUsers(newGlobalSize);

					}

				}

				CombineProcessModel processModel = new CombineProcessModel();
				if (!modelObject.getSendGridCategory().equalsIgnoreCase("") && !modelObject.getSendGridCategory().contains("sparkpost")) {
					processModel.setCategory_or_compaingIds(modelObject.getSendGridCategory().trim());

				} else {
					processModel.setCategory_or_compaingIds(modelObject.getCampaign_id());
				}
				processModel.setDomainName(modelObject.getDomain_name());
				processModel.setTotalNumberOfUsers(size);
				processModel.setUserProvider(modelObject.getProviderFromQueue().trim());
				// new code
				processModel.setSubjectFileName(modelObject.getSubjectFileName());
				processModel.setPostalAddress(modelObject.getPostalAddress().replaceAll("\\|", " "));
				processModel.setImage_postaladdress(modelObject.getImage_postaladdress().replaceAll("\\|", " "));
				processModel.setArborpixel(modelObject.getArborpixel().replaceAll("\\|", ""));
				processModel.setWhite_label(modelObject.getWhite_label().replaceAll("\\|", ""));

				lastList.add(processModel);

				Utility.queueCountHashMap_CombineProcess.put(modelObject.getQueueName().trim(), lastList);

			}
			// Utility.queueCountHashMap.put(modelObject.getQueueName(),
			// lastCount);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/Java_process_stats/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(log_dashboard_folder + File.separator + modelObject.getQueueName() + "_size.txt");
			for (int i = 0; i < lastList.size(); i++) {
				// for parent process
				if (lastList.get(i).getCategory_or_compaingIds().contains("global")) {
					queueData = queueData + lastList.get(i).getTotalNumberOfUsers() + "|" + lastList.get(i).getDomainName() + "|" + lastList.get(i).getCategory_or_compaingIds() + "|" + lastList.get(i).getUserProvider() + "|" + lastList.get(i).getGlobal_campaing_sendgrid() + "\n";

				}
				// for child process
				else {
					queueData = queueData + lastList.get(i).getTotalNumberOfUsers() + "|" + lastList.get(i).getDomainName() + "|" + lastList.get(i).getCategory_or_compaingIds() + "|" + lastList.get(i).getUserProvider() + "|" + lastList.get(i).getSubjectFileName() + "|" + lastList.get(i).getPostalAddress() + "|" + lastList.get(i).getArborpixel() + "|" + lastList.get(i).getWhite_label() + "|" + lastList.get(i).getImage_postaladdress() + "\n";

				}

			}

			try {

				fileWriterObj.write(String.valueOf(queueData));
				System.out.println(queueData);
				fileWriterObj.close();
				System.out.println("written sucessfully at" + log_dashboard_folder + File.separator + modelObject.getQueueName() + "_size.txt");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

		try {
			String fileData = "";
			File file = new File(Utility.baseFilePath + "jobseekerIncrement/updateQueueCount.txt");
			RandomAccessFile fileReadAndWrite = new RandomAccessFile(file, "rw");

			try {
				fileData = fileReadAndWrite.readLine();
				System.out.println("file data=" + fileData);
				int count = Integer.parseInt(fileData);
				count = count + 1;

				// increasing 1000 user after every 2 day
				String writeBacktoFile = "" + count;
				try {
					fileReadAndWrite.seek(0);
					fileReadAndWrite.writeBytes(writeBacktoFile);
					fileReadAndWrite.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

//		Utility.writeDataInCsv_for_whitelable_with_domain_advertisement(Utility.whitelable_and_its_Domain_with_advertisement_stats);

	}

	/**
	 * 
	 * This method we are using to get the total users count of particular query
	 * 
	 * @param query
	 * @param model
	 * @return
	 */
	public static QueueModelObject getDbData(String query, QueueModelObject model) {
		Statement st = null;
		ResultSet res = null;
		try {
			Utility.con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			st = con.createStatement();
			// we are feetching the real data for the real process here mean
			// with para meter

			if (MainClass.queueName_forWhichIsMade != null) {

				for (int k = 0; k < MainClass.queueArray.length; k++) {
					String queueName = MainClass.queueArray[k];
					// means that process name is mentiond
					// we can create the queue with particualr
					if (MainClass.queueArray[k].contains("|")) {
						String testingAray[] = MainClass.queueArray[k].split("\\|");
						queueName = testingAray[0];
						// model.setQueueName(queueName);
						String processName = testingAray[1];
						System.out.println("queuename=" + queueName);
						System.out.println(processName.equalsIgnoreCase(model.getProcessName()) + " " + queueName.equalsIgnoreCase(model.getQueueName()));

						if (processName.equalsIgnoreCase(model.getProcessName()) && queueName.equalsIgnoreCase(model.getQueueName())) {

							model.setQueueUrl(Utility.createQueue(queueName));
							System.out.println(query);
							// here we check for the monthly alerts
							// for
							// the
							// first
							// week of monthly only
							// if
							// (model.getProcessType().contains("monthly"))
							// {
							// if (Utility.getCurrentDayOfMonth() > 6) {
							// model.totalRecordInDb = 0;
							// return model;
							// }
							// }
							res = st.executeQuery(query);
							res.next();
							int totaldataIndb = res.getInt("rowcount");
							Utility.totalUsersInDb = (int) totaldataIndb;
							System.out.println("total data=" + Utility.totalUsersInDb);
							model.totalRecordInDb = Utility.totalUsersInDb;
							return model;

						}

					} else {
						if (queueName.equalsIgnoreCase(model.getQueueName().toLowerCase())) {
							model.setQueueUrl(Utility.createQueue(queueName));
							System.out.println(query);
							// here we check for the monthly alerts for the
							// first
							// week of monthly only
							if (model.getProcessType().contains("monthly")) {
								if (Utility.getCurrentDayOfMonth() > 7) {
									model.totalRecordInDb = 0;
									return model;
								}
							}
							res = st.executeQuery(query);
							res.next();
							int totaldataIndb = res.getInt("rowcount");
							Utility.totalUsersInDb = (int) totaldataIndb;
							System.out.println("total data=" + Utility.totalUsersInDb);
							model.totalRecordInDb = Utility.totalUsersInDb;
							return model;
						}
					}

				}

			}
			// we are feetching the real data for the real process here mean
			// with no para meter skipping now for the oakjob,jobvital and
			// cylcon beacause oakjob
			// are making by combine process true flag
			else {
				model.setQueueUrl(Utility.createQueue(model.getQueueName()));

				// here we check for the monthly alerts for the first week of
				// monthly only
				if (model.getProcessType().contains("monthly")) {
					if (Utility.getCurrentDayOfMonth() > 7) {
						model.totalRecordInDb = 0;
						return model;
					}
				}
				res = st.executeQuery(query);
				res.next();
				int totaldataIndb = res.getInt("rowcount");
				Utility.totalUsersInDb = (int) totaldataIndb;
				System.out.println("total data=" + Utility.totalUsersInDb);
				model.totalRecordInDb = Utility.totalUsersInDb;
				return model;
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// try {
			// con.close();
			// }
			// catch (Exception e2) {
			// }
		}
		model.totalRecordInDb = 0;
		return model;
	}

	/**
	 * This method is getting all the invalid domains from our data base
	 */
	public static void getInvalidDomain() {
		Statement st = null;

		String query = "select * from tbl_email_keywords;";
		try {

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (res != null && !res.isClosed() && res.next()) {
				Utility.inValidDomainList.add(res.getString("keyword").replaceAll("'", "").trim());

			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method is for getting all the invalid keyword from our database
	 */
	public static void getInvalid_Domain_keyword() {
		Statement st = null;
		String query = "select * from tbl_block_words_from_domain;";
		try {

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			st = con.createStatement();
			ResultSet res = st.executeQuery(query);
			while (res != null && !res.isClosed() && res.next()) {

				try {
					Utility.inValidDomain_blcok_keyword_list.add(res.getString("keyword").toLowerCase());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * 
	 * @param columnname
	 * @return getting invalid domains string
	 */
	public static String invalidDomainString() {
		// t1.email NOT LIKE '%uvlink.uvu.edu%' OR

		String invalid = "|";
		// String q = "'";
		for (int i = 0; i < Utility.inValidDomainList.size(); i++) {
			invalid = invalid + Utility.inValidDomainList.get(i).replace("'", "") + "|";
		}

		// if (q.length() > 0)
		// q = q.substring(0, q.length() - 3);

		// if (invalid.length() > 0)
		// invalid = invalid.substring(0, invalid.length() - 2);
		return invalid;
	}

	/**
	 * 
	 * @param quename
	 * @return This method is for creating queue over the aws account
	 */
	public static String createQueue(String quename) {
		String queUrl = "";
		GetQueueUrlResult result = null;
		try {
			System.out.println("queue creation...........");
			// for paper rosen job seekers.........................
			// for rest of seekers.........................
			System.out.println(quename);
			try {
				result = Utility.sqs.getQueueUrl(quename);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				System.out.println("queu does not exist new will be created");
			}
			if (result != null) {
				queUrl = result.getQueueUrl();
				// we are checking the size of that queue
				int size = getQueueSize(queUrl);
				if (size != 0) {
					// here we purge that queue first

					try {
						// if this queue is comes for the first time
						if (MainClass.queueName_forWhichIsMade != null) {
							if (Utility.queueCountHashMap_CombineProcess.get(quename) == null) {
								// CombineProcessModel model = new
								// CombineProcessModel();
								// ArrayList<CombineProcessModel> list = new
								// ArrayList<CombineProcessModel>();
								// list.add(model);
								// Utility.queueCountHashMap_CombineProcess.put(quename,
								// list);
								purgeQueueRequest(queUrl);

							}
						} else {
							if (Utility.queueCountHashMap_CombineProcess.get(quename) == null) {
								// CombineProcessModel model = new
								// CombineProcessModel();
								// ArrayList<CombineProcessModel> list = new
								// ArrayList<CombineProcessModel>();
								// list.add(model);
								// Utility.queueCountHashMap_CombineProcess.put(quename,
								// list);
								purgeQueueRequest(queUrl);
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {
				CreateQueueRequest createQueueRequest = new CreateQueueRequest(quename);
				try {
					// adding attribute to the queue................
					// ==============================================================================================
					Map<String, String> requestQueMap = new HashMap<String, String>();
					requestQueMap.put("VisibilityTimeout", "3600");
					requestQueMap.put("MaximumMessageSize", "262144");
					requestQueMap.put("ReceiveMessageWaitTimeSeconds", "15");
					requestQueMap.put("DelaySeconds", "0");
					createQueueRequest.setAttributes(requestQueMap);
					queUrl = Utility.sqs.createQueue(createQueueRequest).getQueueUrl();
					// ==============================================================================================
				} catch (AmazonServiceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (AmazonServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (AmazonClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return queUrl;
	}

	// this method is used to get rampup files data
	public static String getDataFromfile(String path) {

		// file is using for the double the users after everry 3 days of
		// mail sending
		File file = new File(path);
		String fileData = "";
		try {
			RandomAccessFile fileReadAndWrite = new RandomAccessFile(file, "rw");
			String line = fileReadAndWrite.readLine();

			while (line != null) {

				fileData = fileData + line + "\n";

				line = fileReadAndWrite.readLine();

			}

			System.out.println(fileData);

		} catch (Exception e) {
			e.printStackTrace();

		}

		return fileData;
	}

	// here we are getting the zipcode lati long from city and state
	public static ArrayList<String> getzipcodeLatLongFromCityAndState(String city, String state, Connection con) {
		Statement st;
		ArrayList<String> dataList = new ArrayList<String>();

		try {
			city = city.replaceAll("'", "").trim();
			state = state.replaceAll("'", "").trim();

			Class.forName("com.mysql.jdbc.Driver");
			String query = "";
			if (city != "" && state == "") {

				query = "select * from tbl_zipcodes where primary_city='" + city + "' limit 1";

			} else {

				query = "select * from tbl_zipcodes where primary_city='" + city + "' AND state='" + state.trim() + "' limit 1";

			}

			try {
				// System.out.println(query);
				st = con.createStatement();
				ResultSet res = st.executeQuery(query);
				while (res != null && !res.isClosed() && res.next()) {
					dataList.add(res.getString("zip"));
					dataList.add(res.getString("primary_city"));
					dataList.add(res.getString("state"));
					dataList.add(res.getString("latitude"));
					dataList.add(res.getString("longitude"));

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
				}
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Cannot find the driver in the classpath!", e);
		}
		return dataList;
	}

	// here we are selectign selecting fresh record

	// here we are inserting data fro the jobkernal in the active table
	public static boolean insert_into_Torch_active(int torch_not_gmail_users) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();
			String query = "insert into tbl_job_seekers_torch (priority,first_name,last_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)  " + "select priority,first_name,last_name,email,keyword,zipcode,now(),'0',radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_torch_backup where email not like '%@gmail%' limit 0," + torch_not_gmail_users;

			System.out.println(query);
			int res = st.executeUpdate(query);

			if (res != 0) {

				System.out.println("inserted");
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	// here we are inserting data fro the jobkernal in the active table
	public static boolean insert_gmail_into_Torch_active(int torch_gmail_users) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();
			String query = "insert into tbl_job_seekers_torch (priority,first_name,last_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)  " + "select priority,first_name,last_name,email,keyword,zipcode,now(),'0',radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_torch_backup where email  like '%@gmail%' limit 0," + torch_gmail_users;

			System.out.println(query);
			int res = st.executeUpdate(query);

			if (res != 0) {

				System.out.println("inserted");
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * 
	 * @param deckins_insertCount_new
	 * @param deckins_insertCount_older
	 * @param older_domain_users_for_deckin
	 * @return
	 */
	public static boolean insert_into_bowering_and_deckinc(int deckins_insertCount_new, int deckins_insertCount_older, int older_domain_users_for_deckin) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();
			// String bowering_query =
			// "insert into tbl_job_seekers_bowering(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)"
			// +
			// " select priority,name,email,keyword,zipcode,now(),advertisement,radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_oakjob_Bowering_rampup limit 1,"
			// + bowering_insertCount;

			String deckinc_query_for_new_frehser = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + " select priority,name,email,keyword,zipcode,now(),advertisement,radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + deckins_insertCount_new;

			String deckinc_query_for_old_frehser = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + " select priority,name,email,keyword,zipcode,now(),advertisement,radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_deckinc_older_fresher_backup limit 1," + deckins_insertCount_older;

			// int bowering_resultset = st.executeUpdate(bowering_query);
			if (older_domain_users_for_deckin == 0) {
				int deckinc_resultset_new_frehsers = st.executeUpdate(deckinc_query_for_new_frehser);

				int deckinc_resultset_old_freshers = st.executeUpdate(deckinc_query_for_old_frehser);

				if (deckinc_resultset_old_freshers != 0 && deckinc_resultset_new_frehsers != 0) {

					System.out.println("inserted");
					return true;
				}
			} else {
				String deckinc_query_for_old_frehser_into = "insert into tbl_job_seekers_deckinc_older_fresher(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + " select priority,name,email,keyword,zipcode,now(),advertisement,radius,frequency,click,open,now(),open_at_evening,city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + older_domain_users_for_deckin;
				int deckinc_old_frehser_into_table = st.executeUpdate(deckinc_query_for_old_frehser_into);
				if (deckinc_old_frehser_into_table != 0) {

					System.out.println("inserted");
					return true;
				}

			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param provider
	 * @param last_week
	 * @param upcoming_week_number
	 * @param users_send
	 * @return
	 */
	public static boolean insertRecord_into_momthlytable(String provider, int last_week, int upcoming_week_number, int users_send) {
		try {

			String modified_date = Utility.jobkernalDateformat.format(new Date().getTime()).replace("'", "");
			// System.out.println(query);
			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();
			String query = "INSERT INTO tbl_monthly_users_processed (provider,modified_date,current_week_number,user_send,upcoming_week_number) VALUES ('" + provider + "','" + modified_date + "','" + last_week + "','" + users_send + "','" + upcoming_week_number + "')";

			System.out.println(query);

			int res = st.executeUpdate(query);

			if (res != 0) {

				System.out.println("inserted");
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param provider
	 * @return
	 */
	public static int[] get_monthly_weekNumber(String provider) {
		int week_numbers[] = new int[2];
		week_numbers[0] = 1;
		week_numbers[1] = 1;
		try {
			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();
			String query = "select * from tbl_monthly_users_processed where provider='" + provider + "' order by id desc limit 1";

			System.out.println(query);

			ResultSet res = st.executeQuery(query);
			while (res != null && !res.isClosed() && res.next()) {
				week_numbers[0] = Integer.parseInt(res.getString("current_week_number"));
				week_numbers[1] = Integer.parseInt(res.getString("upcoming_week_number"));
			}
			return week_numbers;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return week_numbers;
	}

	/**
	 * 
	 * 
	 * @return
	 */
	public static String getTheCurrentDayWeek() {
		Calendar myCalendar = Calendar.getInstance();
		myCalendar.setTime(new Date());
		myCalendar.setFirstDayOfWeek(Calendar.MONDAY);
		String day = String.valueOf(myCalendar.get(Calendar.DAY_OF_WEEK) - 1);
		System.out.println("day=" + day);

		return day;
	}

	/**
	 * 
	 * @return
	 */
	public static int getCurrentDayOfMonth() {

		try {

			Calendar cal = Calendar.getInstance();
			int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);

			return dayOfMonth;

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return 10;

	}

	/**
	 * 
	 * @param uptoWhich
	 */
	public static void delete_from_torch_backup_table(int uptoWhich) {
		Statement st = null;
		Connection con = null;
		String query = "";
		try {
			con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			if (con != null) {

				query = "Delete from tbl_job_seekers_torch_backup where email not like '%@gmail%' limit " + uptoWhich;

				st = con.createStatement();
				System.out.println(query);
				boolean res = st.execute(query);
				if (!res)
					System.out.println("Succesfully deleted");
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * 
	 * 
	 * @param uptoWhich
	 */
	public static void delete_torch_gmail_users_backup_table(int uptoWhich) {
		Statement st = null;
		Connection con = null;
		String query = "";
		try {
			con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			if (con != null) {

				query = "Delete from tbl_job_seekers_torch_backup where email like '%@gmail%' limit " + uptoWhich;

				st = con.createStatement();
				System.out.println(query);
				boolean res = st.execute(query);
				if (!res)
					System.out.println("Succesfully deleted");
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * 
	 * @param deleteCount_deckins_newer
	 * @param deleteCount_deckins_older
	 * @param deleteCount_deckins_into_older
	 */
	public static void delete_from_browering_deckins_tables(int deleteCount_deckins_newer, int deleteCount_deckins_older, int deleteCount_deckins_into_older) {
		Statement st = null;
		Connection con = null;
		try {
			con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			if (con != null) {

				// String browering_query =
				// "Delete from tbl_job_seekers_oakjob_Bowering_rampup limit " +
				// deleteCount_bowering;
				st = con.createStatement();
				if (deleteCount_deckins_into_older == 0) {

					String deckins_query_new_frehsers = "Delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + deleteCount_deckins_newer;

					String deckins_query_older_freshers = "Delete from tbl_job_seekers_deckinc_older_fresher limit " + deleteCount_deckins_older;

					// System.out.println(browering_query);
					// boolean browering_res = st.execute(browering_query);
					boolean deckins_res_new_freshers = st.execute(deckins_query_new_frehsers);
					boolean deckins_res_old_freshers = st.execute(deckins_query_older_freshers);

					if (!deckins_res_old_freshers && !deckins_res_new_freshers)
						System.out.println("Succesfully deleted");
				} else {
					String deleteCount_deckins_older_frehser = "Delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + deleteCount_deckins_into_older;
					boolean delete_deckinc_old_frehser_into_table = st.execute(deleteCount_deckins_older_frehser);
					if (!delete_deckinc_old_frehser_into_table) {
						System.out.println("Succesfully deleted");

					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * 
	 * @param query
	 */
	public static void updateTableForUpsAlerts(String query) {
		if (!query.equalsIgnoreCase("")) {
			Statement st = null;
			Connection con = null;
			try {
				con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
				if (con != null) {
					st = con.createStatement();
					System.out.println(query);
					int res = st.executeUpdate(query);

					if (res != 0) {
						System.out.println("Succesfully updated");
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			} finally {
				if (con != null)
					try {
						con.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		}
	}

	/**
	 * 
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public static String[] getupperAndLowerLatitudeANdLONGITUDE(double latitude, double longitude) {
		String[] dataArray = new String[5];

		Double R = (double) 6371; // earth radius in km
		double radius = 34; // km
		// double radius = 28;
		try {
			if (latitude != 0 && longitude != 0) {
				// real
				// get the upper and lower bounds lat-long
				dataArray[0] = String.valueOf(latitude + Math.toDegrees(radius / R));
				dataArray[1] = String.valueOf(longitude - Math.toDegrees(radius / R / Math.cos(Math.toRadians(latitude))));
				dataArray[2] = String.valueOf(latitude - Math.toDegrees(radius / R));
				dataArray[3] = String.valueOf(longitude + Math.toDegrees(radius / R / Math.cos(Math.toRadians(latitude))));

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataArray;

	}

	// code to move the 20000 users of job vitals in the table
	public static void insert2000UserInJObvitals() {
		Statement st = null;
		Connection con = null;
		String query = "";

		try {
			con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			if (con != null) {
				// here we insert record into the table
				query = "insert into tbl_jobvital_seeker (alert_id,name,email,keyword,location,radius,open,tbl_jobvital_seeker.current_date" + ",frequency,ups_alert_count,click) select alert_id,name,email,keyword,location,radius,'0'," + "curdate(),frequency,ups_alert_count,'0' from tbl_matched_jobs where email like '%@gmail.com' limit 20000";
				st = con.createStatement();
				System.out.println(query);
				boolean res = st.execute(query);
				if (!res) {
					System.out.println("Succesfully inserted in tbl_jobvital_seeker");
					deleteFromJob_matchedTable();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
 * 
 */
	public static void deleteFromJob_matchedTable() {
		Statement st = null;
		Connection con = null;
		String query = "";

		try {
			con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			if (con != null) {
				// query = "Delete from " + Utility.jobVitalActiveTable
				// + " where email IN (" + emails + ")";
				query = "delete from tbl_matched_jobs where email like '%@gmail.com' limit 20000";
				st = con.createStatement();
				System.out.println(query);
				// System.exit(0);
				boolean res = st.execute(query);
				if (!res) {
					System.out.println("Succesfully deleted from tbl_matched_jobs");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}

	/**
	 * here we are initializing keyword for memcache
	 */
	public static void initializeMemcacheFor_Oak_TheKeyword() {

		Statement st = null;
		System.out.println("*****************************geeting search keyword **************************************");
		String query = "select * from tbl_oakjob_new_mapping_keywords";
		try {

			Connection con = null;
			try {
				con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (con != null) {
				st = con.createStatement();
				ResultSet res = st.executeQuery(query);

				while (res != null && !res.isClosed() && res.next()) {

					MapKeyWordModel model = new MapKeyWordModel();

					model.setKeyword(res.getString("keyword").trim());
					model.setSearchKeyword(res.getString("searchkeyword").trim());
					if (!model.getKeyword().equalsIgnoreCase("")) {
						char c = model.getKeyword().toUpperCase().charAt(0);
						ArrayList<MapKeyWordModel> list = Utility.oak_white_label_keyword_mapping_map.get(String.valueOf(c));

						if (list == null) {

							list = new ArrayList<MapKeyWordModel>();
							list.add(model);
							Utility.oak_white_label_keyword_mapping_map.put(String.valueOf(c), list);

						} else {

							list.add(model);
							Utility.oak_white_label_keyword_mapping_map.put(String.valueOf(c), list);

						}
					}

				}
				try {
					con.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * For oakjob we are initializing keywords
	 */
	public static void initializesearch_keyword_for_oakjob_newApi() {

		Statement st = null;
		System.out.println("*****************************geeting search keyword **************************************");
		String query = "select * from tbl_oakjob_new_mapping_keywords_testing";
		try {

			Connection con = null;
			try {
				con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (con != null) {
				st = con.createStatement();
				ResultSet res = st.executeQuery(query);

				while (res != null && !res.isClosed() && res.next()) {

					MapKeyWordModel model = new MapKeyWordModel();

					model.setKeyword(res.getString("keyword").trim());
					model.setSearchKeyword(res.getString("searchkeyword").trim());
					if (!model.getKeyword().equalsIgnoreCase("")) {
						char c = model.getKeyword().toUpperCase().charAt(0);
						ArrayList<MapKeyWordModel> list = Utility.oak_job_keyword_mapping_map.get(String.valueOf(c));

						if (list == null) {

							list = new ArrayList<MapKeyWordModel>();
							list.add(model);
							Utility.oak_job_keyword_mapping_map.put(String.valueOf(c), list);

						} else {

							list.add(model);
							Utility.oak_job_keyword_mapping_map.put(String.valueOf(c), list);

						}
					}

				}
				try {
					con.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * 
	 * @param jsonArray
	 *            we are generating queue model list here
	 */
	public static void genrateQueuemodelList(JSONArray jsonArray) {
		// TODO Auto-generated method stub

		if (jsonArray != null && jsonArray.length() > 0) {

			QueueModelObject[] queuesArray = new Gson().fromJson(jsonArray.toString(), QueueModelObject[].class);

			Utility.queuesArrayList = (List<QueueModelObject>) Arrays.asList(queuesArray);

			for (int i = 0; i < Utility.queuesArrayList.size(); i++) {

				if (Utility.queuesArrayList.get(i).getIsActive().equalsIgnoreCase("1")) {

					String queueArray[] = Utility.queuesArrayList.get(i).getQueueName().split("\\|");
					for (int j = 0; j < queueArray.length; j++) {

						Utility.numberOfActiveQueueCount++;
						// gettting last count for the queue and incrementing it
						// by one
						Integer lastQueue_Count = queue_AndTheir_count_hashMap.get(queueArray[j]);
						if (lastQueue_Count == null) {
							queue_AndTheir_count_hashMap.put(queueArray[j], 1);
						} else {
							queue_AndTheir_count_hashMap.put(queueArray[j], lastQueue_Count + 1);

						}
					}
				}

			}

			System.out.println(Utility.queuesArrayList.size());

		}

	}

	/**
	 * 
	 * @param queueCountHashMap_CombineProcess
	 *            here we are sending sms
	 */
	public static void sendTextSms(HashMap<String, ArrayList<CombineProcessModel>> queueCountHashMap_CombineProcess) {
		try {
			String phoneNumber = "8860547656,8699864636,9855115882";
			// Encoding data in the utf-8

			try {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(baseFilePath + "update_server_count/phonenumberlist.txt"));
				String line = bufferedReader.readLine();
				if (line != null)
					phoneNumber = line;
				bufferedReader.close();
			} catch (Exception e) {
			}

			String data = "";
			if (queueCountHashMap_CombineProcess.size() == 1) {
				// "Today " + numberOfQueues +
				// " has been created FROM DASHBOARD..."
				Iterator it = Utility.queueCountHashMap_CombineProcess.entrySet().iterator();
				while (it.hasNext()) {

					Map.Entry pair = (Map.Entry) it.next();
					data = "Today " + pair.getKey() + " queue has been created";
				}
			} else {
				data = "Today " + queueCountHashMap_CombineProcess.size() + " has been created FROM DASHBOARD...";

			}

			data = URLEncoder.encode(data, "UTF-8");

			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";

			url = url + phoneNumber;

			String text = "&text=" + data;

			url = url + text;

			url = url + "&priority=ndnd&stype=normal";

			System.out.println("Url=" + url);

			URL obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");

				System.out.println("\nSending 'GET' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
			}
			// print result
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param lat1
	 * @param lng1
	 * @return
	 */
	public static double distFrom(String lat1, String lng1) {

		// lat and long of ups -new feed zipcode=40202

		double lat2 = (float) 38.22;
		double lng2 = (float) -85.74;

		// ====================//===============================

		double earthRadius = 6371000; // meters

		double dLat = Math.toRadians(lat2 - Float.valueOf(lat1));

		double dLng = Math.toRadians(lng2 - Float.valueOf(lng1));
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(Float.valueOf(lat1))) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		// getting dis in meters to km
		double dist = (earthRadius * c) / 1000;
		// returning it in miles from km

		dist = dist * 0.621371;
		return Math.round(dist * 100.0) / 100.0;
	}

	/**
	 * 
	 * @param path
	 * @return
	 */
	public static String readLastUserProcess(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			System.out.println(out.toString());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// from input stream

		return out.toString();
	}

	/**
	 * 
	 * @param filePath
	 * @param data
	 */
	public static void writestatsBackToTheFile(String filePath, String data) {

		BufferedWriter bw = null;
		try {
			// Specify the file name and path here
			File file = new File(filePath);

			/*
			 * This logic will make sure that the file gets created if it is not
			 * present at the specified location
			 */
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			bw.write(String.valueOf(data));
			System.out.println("File written Successfully");

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
			} catch (Exception ex) {
				System.out.println("Error in closing the BufferedWriter" + ex);
			}
		}
	}

	/**
	 * 
	 * @param model
	 * @return
	 */
	public static String get_domain_wise_userstats(QueueModelObject model) {

		Iterator it = Utility.domainWiseUsersStats.entrySet().iterator();
		while (it.hasNext()) {

			Map.Entry pair = (Map.Entry) it.next();
			// alumnirecruiter.com_bravemail.uncp.edu
			String domainKeyArray[] = pair.getKey().toString().split("_");
			// at [0] index we have process name
			// at [1] we have domain
			// and [2] we have emaildomain
			DomainWiseUserStats domainWiseModel = Utility.preExistingHashMap_DomainWiseHashMap.get(domainKeyArray[0].toLowerCase().trim() + "_" + domainKeyArray[1].toLowerCase().trim());
			//

			System.out.println(domainKeyArray[0] + "," + domainKeyArray[1] + "," + domainKeyArray[2] + "," + pair.getValue().toString());
			System.out.println("domainWiseModel=" + domainWiseModel + " and key from getting=" + domainKeyArray[0].toLowerCase().trim());

			if (domainWiseModel == null) {
				domainWiseModel = new DomainWiseUserStats();
				domainWiseModel.setProcesSName(domainKeyArray[0]);
				domainWiseModel.setGmailCount("0");
				domainWiseModel.setYahooCount("0");
				domainWiseModel.setAolCount("0");
				domainWiseModel.setOthersCount("0");

			}
			// else if
			// (Utility.queueCountHashMap_CombineProcess.get(model.getQueueName().trim())
			// == null) {
			//
			// domainWiseModel = new DomainWiseUserStats();
			// domainWiseModel.setProcesSName(domainKeyArray[0]);
			// domainWiseModel.setGmailCount("0");
			// domainWiseModel.setYahooCount("0");
			// domainWiseModel.setAolCount("0");
			// domainWiseModel.setOthersCount("0");
			//
			// }
			if (domainKeyArray[2].toLowerCase().contains("gmail")) {

				Integer lastOtherCount = Integer.parseInt(domainWiseModel.getGmailCount());
				lastOtherCount = lastOtherCount + Integer.parseInt(pair.getValue().toString());
				domainWiseModel.setGmailCount(lastOtherCount + "");

			} else if (domainKeyArray[2].toLowerCase().contains("yahoo")) {

				Integer lastOtherCount = Integer.parseInt(domainWiseModel.getYahooCount());
				lastOtherCount = lastOtherCount + Integer.parseInt(pair.getValue().toString());
				domainWiseModel.setYahooCount(lastOtherCount + "");

			} else if (domainKeyArray[2].toLowerCase().contains("aol")) {

				Integer lastOtherCount = Integer.parseInt(domainWiseModel.getAolCount());
				lastOtherCount = lastOtherCount + Integer.parseInt(pair.getValue().toString());
				domainWiseModel.setAolCount(lastOtherCount + "");

			} else {

				Integer lastOtherCount = Integer.parseInt(domainWiseModel.getOthersCount());
				lastOtherCount = lastOtherCount + Integer.parseInt(pair.getValue().toString());
				domainWiseModel.setOthersCount(lastOtherCount + "");

			}
			Utility.preExistingHashMap_DomainWiseHashMap.put(domainKeyArray[0].toLowerCase().trim() + "_" + domainKeyArray[1].toLowerCase().trim(), domainWiseModel);
			System.out.println("domainKeyArray[0].toLowerCase().trim()" + domainKeyArray[0].toLowerCase().trim());

			it.remove(); // avoids a ConcurrentModificationException

			// new code to insert in the data base as the domain wise
		}

		// here we are returning the jason model object

		String gson = new Gson().toJson(Utility.preExistingHashMap_DomainWiseHashMap);
		// System.out.println(gson);
		return gson;

	}

	/**
	 * 
	 * @param queueModel
	 * @return This method is used to write stats for other process
	 */
	public static String get_ProcessWise_userStats(QueueModelObject queueModel) {
		Iterator it = Utility.processWiseUserstats_map.entrySet().iterator();

		while (it.hasNext()) {

			Map.Entry pair = (Map.Entry) it.next();

			String key = pair.getKey().toString();
			ProcessWiseUserModel model = (ProcessWiseUserModel) pair.getValue();

			ProcessWiseUserModel lastProcessModel = Utility.preExistingHashMap_ProcessWise_HashMap.get(key);

			if (lastProcessModel == null) {
				Utility.preExistingHashMap_ProcessWise_HashMap.put(key, model);

			} else if (Utility.queueCountHashMap_CombineProcess.get(queueModel.getQueueName().trim()) == null) {

				Utility.preExistingHashMap_ProcessWise_HashMap.put(key, model);

			} else {

				Utility.preExistingHashMap_ProcessWise_HashMap.put(key, model);
			}

		}

		String jasonData = new Gson().toJson(Utility.preExistingHashMap_ProcessWise_HashMap);
		// System.out.println(jasonData);
		return jasonData;

	}

	/**
	 * 
	 * @param model
	 */
	public static synchronized void write_processWise_user_Stats_infile(QueueModelObject model) {
		File log_dashboard_folder = null, dataFile = null;
		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/ProcessWiseUserStats/");
			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// this is for oakjob and jobvitals

			dataFile = new File(log_dashboard_folder + File.separator + "process_wise_user_" + Utility.dateFormat.format(new Date()) + ".json");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {

				fileWriterObj.write(Utility.get_ProcessWise_userStats(model));
				fileWriterObj.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	/**
	 * 
	 * @param model
	 */
	public static synchronized void write_domainWise_user_Stats_infile(QueueModelObject model) {
		File log_dashboard_folder = null, dataFile = null;
		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/DomainWiseUsers/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			dataFile = new File(log_dashboard_folder + File.separator + "domain_wise_user_" + Utility.dateFormat.format(new Date()) + ".json");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {

				fileWriterObj.write(Utility.get_domain_wise_userstats(model));
				fileWriterObj.close();
				// System.out.println("written sucessfully at" +
				// log_dashboard_folder + File.separator + "domain_wise_user_" +
				// Utility.dateFormat.format(new Date()) + ".json");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	/**
	 * 
	 * @param queueUrl
	 * @return
	 */
	public static int getQueueSize(String queueUrl) {

		ArrayList<String> attrList = new ArrayList<String>();
		attrList.add("All");
		attrList.add("ApproximateNumberOfMessages");
		GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(queueUrl);
		getQueueAttributesRequest.setAttributeNames(attrList);
		GetQueueAttributesResult result = Utility.sqs.getQueueAttributes(getQueueAttributesRequest);

		int size = 0;

		try {
			size = Integer.parseInt(result.getAttributes().get("ApproximateNumberOfMessages"));
			System.out.println("getting the size of queue");
		} catch (Exception e) {

		}

		return size;

	}

	/**
	 * 
	 * @param queUrl
	 *            This method is used to purge the queue
	 */
	public static void purgeQueueRequest(String queUrl) {
		PurgeQueueRequest purgeRequest = new PurgeQueueRequest(queUrl);
		Utility.sqs.purgeQueue(purgeRequest);
		System.out.println("purging the preexisted queue");

	}

	public static HashMap<String, String> mailTesterUsersMap = new HashMap<String, String>();

	public static String torch_gmail_rampup;

	public static void writeMailtesterUserInfo() {

		try {
			File log_dashboard_folder = new File(Utility.filePath + "/mis_logs/MailTester/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}

			try {
				FileWriter fileWriterObj = null;
				fileWriterObj = new FileWriter(log_dashboard_folder + File.separator + "mail-tester-users" + dateFormat.format(new Date()) + ".json");
				fileWriterObj.write(new Gson().toJson(Utility.mailTesterUsersMap));
				fileWriterObj.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Here we are writitng searched keyword
	 */
	public static synchronized void write_searchKeyword_FoundStats() {
		File log_dashboard_folder = null, dataFile = null;
		;
		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/SearchKeyword/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// this is for oakjob and jobvitals

			dataFile = new File(log_dashboard_folder + File.separator + "process_wise_user_" + Utility.dateFormat.format(new Date()) + ".json");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {
				String jasonData = new Gson().toJson(Utility.searchKeywordAndLengthCount);

				fileWriterObj.write(jasonData);
				fileWriterObj.close();
				System.out.println("written sucessfully at" + log_dashboard_folder + File.separator + "process_wise_user_" + Utility.dateFormat.format(new Date()) + ".json");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	// here we are inserting data fro the jobkernal in the active table
	public static boolean rampup_for_the_jobkernel(int jobkalerts_count, int jobkernel_com_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String jobkernel_com_count_query = "insert into tbl_job_seekers_jobskernal_freshers_rampup(priority,first_name,last_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + "select priority,first_name,last_name,email,keyword,zipcode,now(),'',30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_jobskernal_engagedusers_rampup limit 1," + jobkernel_com_count;

			String jobkalerts_count_query_first = "insert into tbl_job_seekers_jobskernal(priority,first_name,last_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + "select priority,first_name,last_name,email,keyword,zipcode,now(),'',30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_jobskernal_engagedusers_rampup limit 1," + jobkalerts_count;

			String jobkalerts_count_query_second = "insert into tbl_job_seekers_jobskernal(priority,first_name,last_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + "select priority,first_name,last_name,email,keyword,zipcode,now(),'',30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_jobskernal_freshers_rampup limit 1," + jobkalerts_count;

			String delete_jobkernel_count_query_first = "delete from tbl_job_seekers_jobskernal_engagedusers_rampup limit " + jobkernel_com_count;

			String delete_jobkalerts_count_query_first = "delete from tbl_job_seekers_jobskernal_engagedusers_rampup limit " + jobkalerts_count;

			String delete_jobkalerts_count_query_second = "delete from tbl_job_seekers_jobskernal_freshers_rampup limit " + jobkalerts_count;

			int is_jobkernel_com_count_inserted = st.executeUpdate(jobkernel_com_count_query);
			System.out.println(jobkernel_com_count_query);
			if (is_jobkernel_com_count_inserted != 0) {

				boolean is_deleted = st.execute(delete_jobkernel_count_query_first);
				// it will run only after the every 2nd day as per the rampup
				System.out.println(delete_jobkernel_count_query_first);
				if (!is_deleted && jobkalerts_count != 0) {

					int jobkalerts_count_query_first_result = st.executeUpdate(jobkalerts_count_query_first);
					System.out.println(jobkalerts_count_query_first);
					int jobkalerts_count_query_second_result = st.executeUpdate(jobkalerts_count_query_second);
					System.out.println(jobkalerts_count_query_second);

					if (jobkalerts_count_query_first_result != 0 && jobkalerts_count_query_second_result != 0) {

						boolean is_deleted_for_first = st.execute(delete_jobkalerts_count_query_first);
						System.out.println(delete_jobkalerts_count_query_first);
						boolean is_deleted_for_second = st.execute(delete_jobkalerts_count_query_second);
						System.out.println(delete_jobkalerts_count_query_second);
						if (!is_deleted_for_first && !is_deleted_for_second) {
							System.out.println("executed successfully");
							return true;

						}

					}
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	/**
	 * 
	 * @param userDataObject
	 * @return this method is used to create dynamic campaign in mailgun
	 */
	public static String CreateCampaign(QueueModelObject userDataObject) {
		Client client = new Client();
		client.addFilter(new HTTPBasicAuthFilter("api", "key-b8f6462082ffa67a3a56352b14f91523"));
		WebResource webResource = client.resource("https://api.mailgun.net/v3/" + userDataObject.getDomain_name() + "/campaigns");
		MultivaluedMapImpl formData = new MultivaluedMapImpl();
		formData.add("name", userDataObject.getProcessType());
		formData.add("id", userDataObject.getCampaign_id());
		return webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData).toString();
	}

	/**
	 * Writing domain and campaign wise stats
	 */
	public static synchronized void write_domain_campaign_wise_status() {
		File log_dashboard_folder = null, dataFile = null;
		;
		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/Domain_Compaign/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// this is for oakjob and jobvitals

			dataFile = new File(log_dashboard_folder + File.separator + "domain_compaign" + ".json");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {

				String jasonData = new Gson().toJson(Utility.domain_campaign_status_map);
				fileWriterObj.write(jasonData);
				fileWriterObj.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	/**
	 * 
	 * @param path
	 * @return Here we initialize domain wise campaign wise hash map
	 */
	public static String initialize_domain_campaign_wise_map(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			JSONObject jsonString = new JSONObject(out.toString());
			Utility.domain_campaign_status_map = new Gson().fromJson(jsonString.toString(), new TypeToken<HashMap<String, String>>() {
			}.getType());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// from input stream

		return out.toString();
	}

	// here we are inserting data fro the jobkernal in the active table
	public static boolean rampup_for_the_windward(int windward_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String WINDWARDALERTS_com_count_query = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,country,main_provider)" + "select '1',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,country,main_provider from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + windward_count;

			String WINDWARDJOBALERTS_count_query_first = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,country,main_provider)" + "select '2',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,country,main_provider from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + windward_count;

			String WINDWARDJOBS_count_query_second = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,country,main_provider)" + "select '3',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,country,main_provider from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + windward_count;

			String delete_WINDWARDALERTS_count_query_first = "delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + windward_count;

			String delete_WINDWARDJOBALERTS_count_query_first = "delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + windward_count;

			String delete_WINDWARDJOBS_count_query_second = "delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + windward_count;

			int WINDWARDALERTS_com_count_query_deleted = st.executeUpdate(WINDWARDALERTS_com_count_query);
			System.out.println(WINDWARDALERTS_com_count_query);
			if (WINDWARDALERTS_com_count_query_deleted != 0) {

				boolean WINDWARDALERTS_count_query__deleted = st.execute(delete_WINDWARDALERTS_count_query_first);
				System.out.println(delete_WINDWARDALERTS_count_query_first);
				// it will run only after the every 2nd day as per the rampup
				if (!WINDWARDALERTS_count_query__deleted) {

					int WINDWARDJOBALERTS_count_query_first_result = st.executeUpdate(WINDWARDJOBALERTS_count_query_first);
					System.out.println(WINDWARDJOBALERTS_count_query_first);

					if (WINDWARDJOBALERTS_count_query_first_result != 0) {

						boolean WINDWARDJOBALERTS_count_query_delted = st.execute(delete_WINDWARDJOBALERTS_count_query_first);
						System.out.println(delete_WINDWARDJOBALERTS_count_query_first);
						if (!WINDWARDJOBALERTS_count_query_delted) {
							int WINDWARDJOBS_count_query_second_result = st.executeUpdate(WINDWARDJOBS_count_query_second);
							System.out.println(WINDWARDJOBS_count_query_second);
							if (WINDWARDJOBS_count_query_second_result != 0) {
								boolean WINDWARDJOBS_count_query_second_delted = st.execute(delete_WINDWARDJOBS_count_query_second);
								System.out.println(delete_WINDWARDJOBS_count_query_second);

								if (!WINDWARDJOBS_count_query_second_delted) {
									return true;
								}
							}

						}

					}
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	// here we are inserting data fro the jobkernal in the active table
	public static boolean rampup_for_the_linkus(int linkus_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String linkus_count_query = "insert into tbl_job_seekers_linkus(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone)" + "select priority,first_name,email,keyword,zipcode,now(),'',30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone from tbl_job_seekers_linkus_rampup limit 1," + linkus_count;
			System.out.println("query=" + linkus_count_query);

			String delete_linkus_count_query_second = "delete from tbl_job_seekers_linkus_rampup limit " + linkus_count;

			int linkus_count_query_deleted = st.executeUpdate(linkus_count_query);
			if (linkus_count_query_deleted != 0) {

				boolean delete_linkus_count_query_result = st.execute(delete_linkus_count_query_second);
				// it will run only after the every 2nd day as per the rampup
				if (!delete_linkus_count_query_result) {

					return true;
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	// here we are inserting data fro the jobkernal in the active table
	public static boolean rampup_for_the_tierone(int tierone_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String tierone_count_query = "insert into tbl_job_seekers_tierone(priority,first_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,provider)" + "select '1',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,'' from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + tierone_count;

			String delete_tierone_count_query_first = "delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + tierone_count;

			int tierone_count_query_resut = st.executeUpdate(tierone_count_query);

			if (tierone_count_query_resut != 0) {

				boolean delete_tierone_count_query_result = st.execute(delete_tierone_count_query_first);

				if (!delete_tierone_count_query_result) {
					return true;
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	// here we are inserting data fro the deck2 in the active table
	public static boolean rampup_for_the_deck2(int deck2_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String deck2_count_query = "insert into tbl_job_seekers_deckinc(priority,name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,phone)" + "select '1',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,phone from tbl_job_seekers_deck2 limit 1," + deck2_count;

			String delete_deck2_count_query_first = "delete from tbl_job_seekers_deck2 limit " + deck2_count;

			int deck2_count_query_resut = st.executeUpdate(deck2_count_query);

			if (deck2_count_query_resut != 0) {

				boolean delete_deck2_count_query_result = st.execute(delete_deck2_count_query_first);

				if (!delete_deck2_count_query_result) {
					return true;
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public static void write_all_Provider_names() {

		File log_dashboard_folder = null, dataFile = null;

		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/Providers/");

			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {

			dataFile = new File(log_dashboard_folder + File.separator + "provider_names" + ".txt");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {

				String provider_data = "";
				for (String string : Utility.provider_Sets) {

					provider_data = provider_data + string + "\n";

				}

				fileWriterObj.write(provider_data);
				fileWriterObj.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	public static void iniTialize_map_for_421_users() {

		Statement st = null;
		System.out.println("*****************************421_users **************************************");
		String query = "select * from tbl_events_421  where date(date)>now()-interval 2 day";
		try {

			Connection con = null;
			try {
				con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (con != null) {
				st = con.createStatement();
				ResultSet res = st.executeQuery(query);
				String key = "";
				while (res != null && !res.isClosed() && res.next()) {
					key = res.getString("whitelabel").trim() + "-" + res.getString("email").trim();
					Utility.map_with_421_error_users.put(key, true);

				}
				try {
					con.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void initialize_set_for_providers_name(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {

				Utility.provider_Sets.add(line.trim());

			}

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// from input stream

	}

	// here we are inserting data fro the deckinc in the active table
	public static boolean rampup_for_the_diamond(int diamond_count) {
		try {
			// System.out.println(query);

			Connection con = DriverManager.getConnection(Utility.url, Utility.username, Utility.password);
			Statement st = con.createStatement();

			String diamond_count_query = "insert into tbl_job_seekers_diamond(priority,first_name,email,keyword,zipcode,date,advertisement,radius,frequency,click,open,open_at,open_at_evening,city,state,latitude,longitude,zipcode_timezone,provider,country,main_provider)" + "select '1',name,email,keyword,zipcode,now(),advertisement,30,1,0,0,now(),now(),city,state,latitude,longitude,zipcode_timezone,'',country,main_provider from tbl_job_seekers_oakjob_DECKINC_rampup limit 1," + diamond_count;
			System.out.println(diamond_count_query);

			String delete_diamond_count_query_first = "delete from tbl_job_seekers_oakjob_DECKINC_rampup limit " + diamond_count;

			System.out.println(delete_diamond_count_query_first);
			int diamond_count_query_resultset = st.executeUpdate(diamond_count_query);

			if (diamond_count_query_resultset != 0) {

				boolean delete_diamond_count_query_result = st.execute(delete_diamond_count_query_first);

				if (!delete_diamond_count_query_result) {
					return true;
				}

			}

			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	public static void convert(ResultSet rs) {
		try {
			JSONArray array = new JSONArray();
			ResultSetMetaData rsmd = rs.getMetaData();

			while (rs.next()) {

				JSONObject jsonObj = new JSONObject();
				int numColumns = rsmd.getColumnCount();

				for (int i = 1; i < numColumns + 1; i++) {
					String column_name = rsmd.getColumnName(i);

					try {

						if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
							jsonObj.put(column_name, rs.getArray(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
							jsonObj.put(column_name, rs.getBoolean(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
							jsonObj.put(column_name, rs.getBlob(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
							jsonObj.put(column_name, rs.getDouble(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
							jsonObj.put(column_name, rs.getFloat(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.INTEGER) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
							jsonObj.put(column_name, rs.getNString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
							jsonObj.put(column_name, rs.getString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.TINYINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
							jsonObj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
							jsonObj.put(column_name, rs.getDate(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
							jsonObj.put(column_name, rs.getTimestamp(column_name));
						} else {
							jsonObj.put(column_name, rs.getObject(column_name));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				array.put(jsonObj);

			}

			RampUpModel[] rampUplist = new Gson().fromJson(array.toString(), RampUpModel[].class);
			Utility.rampUpModelist.addAll(Arrays.asList(rampUplist));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static RampUpModel readRampupFromDb() {

		try {
			if (Utility.con == null || Utility.con.isClosed())
				Utility.con = openConnection();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ResultSet rs = null;
		RampUpModel modelClassObject = null;
		String query = "select * from tbl_rampup_process where date(date)=date(now()) and is_rampup_live ='1' and today_rampup_status='FALSE'";
		System.out.println("query==>" + query);

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			rs = pst.executeQuery();

			if (rs != null) {

				convert(rs);
				for (int i = 0; i < Utility.rampUpModelist.size(); i++) {

					System.out.println(Utility.rampUpModelist.get(i).getProcessName());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelClassObject;
	}

	public static Connection openConnection() {
		String url1 = "jdbc:mysql://" + HOST_NAME + "/" + DB_NAME;
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = (Connection) DriverManager.getConnection(url1, username, password);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return con;
	}

	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch (Exception e) {
		}
	}

	public static void perFormRampupAndUpdateInDataBase(RampUpModel model) {
		boolean shouldRampUp_Perform = false;
		int number_of_users_count_to_insert = 0;
		String query = "", insert_query = "", select_query = "", select_countquery = "";
		String delete_query = "";
		String whereCondition = "";
		String import_rampup_backup_table = "tbl_job_seekers_common_import_rampup";
		String import_rampup_main_table = "tbl_job_seekers_common_import";

		String import_users_rampup_backup_query = "insert into " + import_rampup_backup_table + " (priority,first_name,last_name,email,keyword,zipcode,advertisement,city,state,latitude,longitude,zipcode_timezone,main_provider,country,provider,advertisement_for_code)" + "select priority,first_name,last_name,email,keyword,zipcode,advertisement,city,state,latitude,longitude,zipcode_timezone,main_provider,country,provider,advertisement_for_code from " + import_rampup_main_table;
		try {
			if (Utility.con == null || Utility.con.isClosed()) {
				Utility.con = openConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		insert_query = "insert into " + model.getTo_table_name() + " (priority,first_name,last_name,email,keyword,zipcode,advertisement,city,state,latitude,longitude,zipcode_timezone,main_provider,country,affiliate_code,provider)";

		select_query = " select priority,first_name,last_name,email,keyword,zipcode,advertisement,city,state,latitude,longitude,zipcode_timezone,main_provider,country,affiliate_code,provider from " + model.getFrom_table_name();

		if (model.getFrom_table_name().contains("deckinc")) {
			if (model.getTo_table_name().contains("bestpractice") || model.getTo_table_name().contains("linkus")) {
				insert_query = insert_query.replaceAll("first_name,last_name", "name");
				select_query = select_query.replaceAll("first_name,last_name", "name");

			} else if (model.getTo_table_name().contains("dart") || model.getTo_table_name().contains("jobskernal") || model.getTo_table_name().contains("tierone")) {
				insert_query = insert_query.replaceAll("first_name,last_name", "first_name");
				select_query = select_query.replaceAll("first_name,last_name", "name");
			}

		} else if (model.getTo_table_name().contains("blaze") || model.getTo_table_name().contains("linkus") || (model.getFrom_table_name().contains("d4") && model.getProcessName().equalsIgnoreCase("windward")) || model.getTo_table_name().contains("lempop")) {
			insert_query = insert_query.replaceAll("first_name,last_name", "name");
			select_query = select_query.replaceAll("first_name,last_name", "first_name");
		}

		else if (model.getFrom_table_name().contains("jobvital_seeker")) {
			insert_query = insert_query.replaceAll("first_name,last_name", "first_name");
			select_query = select_query.replaceAll("first_name,last_name", "name");

			select_query = select_query.replaceAll(",zipcode,", ",location,");
		}

		// we are pickiing affilifate code for senza now
		// if (!model.getProcessName().toLowerCase().contains("senza")) {

		insert_query = insert_query.replace(",affiliate_code", "");

		select_query = select_query.replace(",affiliate_code", "");

		// }

		if (model.getProcessName().toLowerCase().contains("elitejobalerts") && model.getTo_table_name().contains("tbl_job_seekers_numberonejobsite")) {
			insert_query = insert_query.replace("main_provider", "main_provider,provider");

			select_query = select_query.replace("main_provider", "main_provider,provider");

		}

		if (model.getTo_table_name().contains("tbl_jobvital_seeker")) {
			insert_query = insert_query.replace("priority,first_name,last_name", "name");
			select_query = select_query.replace("priority,first_name,last_name", "name");

			insert_query = insert_query.replace("zipcode,", "location,");
			select_query = select_query.replace("zipcode,", "location,");

		}

		// for advertisement wise ramupu
		if (model.isIs_advertise_use()) {
			if (model.getFrom_table_name().equalsIgnoreCase("tbl_job_seekers_common_import")) {
				whereCondition = " where advertisement ='" + model.getRampup_advertisement() + "'" + " or advertisement_for_code = '" + model.getRampup_advertisement() + "'";

			} else {
				whereCondition = " where advertisement ='" + model.getRampup_advertisement() + "'";

			}

		}

		// for priority wise rampup
		if (model.isIs_priority_use()) {

			int priority = model.getRampup_priority();
			// if (model.getProcessName().equalsIgnoreCase("torchalerts")) {
			//
			// select_query = select_query
			// .replace("priority", "" + model.getRampup_priority());
			//
			// priority = 4;
			//
			// }

			// FOR ALMANAC PRIORITY =1 MEANS almanacdailyalerts.COM AND PRIORITY
			// =2 =almanacjobalerts.COM
			if (model.getProcessName().toLowerCase().contains("weekly")) {
				select_query = select_query.replace("priority", "" + model.getRampup_priority());

			} else {

				if (!whereCondition.equalsIgnoreCase("")) {
					whereCondition = whereCondition + " and priority='" + priority + "'";

				} else
					whereCondition = " where priority='" + priority + "'";
			}

		}

		if (!model.getProvider().equalsIgnoreCase("")) {

			select_query = select_query.replace(",provider", ",'" + model.getProvider() + "'");
		}

		if (model.getType_of_users().equalsIgnoreCase("o/c")) {

			if (!whereCondition.equalsIgnoreCase(""))
				whereCondition = whereCondition + " and open='1' or click='1'";
			else
				whereCondition = " where open='1' or click='1'";

		}

		if (model.isDatecondition_use()) {

			if (!whereCondition.equalsIgnoreCase(""))
				whereCondition = whereCondition + " and date(date)>now()-interval " + model.getDayCount() + " day";
			else
				whereCondition = " where date(date)>now()-interval " + model.getDayCount() + " day";

		}

		if (!model.getUser_domain().equalsIgnoreCase("") && !model.getUser_domain().toLowerCase().equalsIgnoreCase("all")) {
			if (!whereCondition.equalsIgnoreCase(""))
				whereCondition = whereCondition + " and " + model.getUser_domain();
			else
				whereCondition = " where " + model.getUser_domain();

		}

		// here we are fetching data on the basis of email asc
		if (!whereCondition.equalsIgnoreCase(""))
			whereCondition = whereCondition + " order by email asc";
		else
			whereCondition = " order by email asc";
		// ============================================//==================================

		if (Integer.parseInt(model.getRamp_users_count()) >= 24000

		&& !model.getRamp_up_type().equalsIgnoreCase("fixed")) {

			Utility.sendSms("Ramp up count is goes more than 25000 for whitelabel " + model.getProcessName() + " As discuss with kanav. we are impoting 25000 fixed count!!");
			model.setRamp_up_type("fixed");
			model.setRamp_users_count("25000");
			model.setRamp_up_day_count("0");

		}
		int oldRamp_upcount = 0;
		if (model.getRamp_up_day_count().equalsIgnoreCase("0")) {
			shouldRampUp_Perform = true;
			oldRamp_upcount = Integer.parseInt(model.getRamp_users_count());

			if (model.getRamp_up_type().toLowerCase().contains("mailgun")) {
				int newRamp_count = 0;
				if (model.getProcessName().equalsIgnoreCase("jobvitals")) {

					newRamp_count = oldRamp_upcount + 10000;

				} else {
					oldRamp_upcount = Integer.parseInt(model.getRamp_users_count());
					newRamp_count = oldRamp_upcount * 2;

				}

				number_of_users_count_to_insert = oldRamp_upcount * 2 + ((oldRamp_upcount * 25) / 100);

				model.setRamp_users_count(String.valueOf(newRamp_count));

				model.setRamp_up_day_count("0");
			} else if (model.getRamp_up_type().equalsIgnoreCase("percentage")) {
				int usersPercentageCount = Integer.parseInt(model.getRamp_users_count()) * model.getPercent() / 100;
				model.setRamp_users_count(String.valueOf(Integer.parseInt(model.getRamp_users_count()) + usersPercentageCount));
				model.setRamp_up_day_count("0");
				number_of_users_count_to_insert = Integer.parseInt(model.getRamp_users_count());

			}
			// settup for the fixed amount and daily
			else if (model.getRamp_up_type().equalsIgnoreCase("fixed")) {

				model.setRamp_users_count(String.valueOf(Integer.parseInt(model.getRamp_users_count())));
				model.setRamp_up_day_count("0");
				number_of_users_count_to_insert = Integer.parseInt(model.getRamp_users_count());

			}

			// ADDING LIMIT IN THE QUERY
			query = insert_query + select_query;

			query = query + whereCondition + " limit 1," + number_of_users_count_to_insert;
			import_users_rampup_backup_query = import_users_rampup_backup_query + whereCondition + " limit 1," + number_of_users_count_to_insert;
			// if (model.getIs_move_or_copy().equalsIgnoreCase("c")) {
			// query = query + whereCondition + " limit " +
			// model.getStarting_index() + "," +
			// number_of_users_count_to_insert;
			// model.setStarting_index("" + number_of_users_count_to_insert);
			// } else {
			// query = query + whereCondition + " limit 1," +
			// number_of_users_count_to_insert;
			// }

		} else {
			model.setRamp_up_day_count(Integer.parseInt(model.getRamp_up_day_count()) - 1 + "");
			shouldRampUp_Perform = false;

		}

		Statement st = null;

		// special handling of jobvital rampup

		try {
			select_countquery = "select count(*) as rowcount from " + model.getFrom_table_name();
			select_countquery = select_countquery + " " + whereCondition;
			System.out.println(select_countquery);
			st = Utility.con.createStatement();
			ResultSet rs;
			try {
				rs = st.executeQuery(select_countquery);
				if ((rs == null || !rs.next()) && (!model.getProcessName().toLowerCase().contains("weekly"))) {
					System.out.println("0 result found");
					shouldRampUp_Perform = false;
					Utility.sendEmailJustForAlerts("ALERT!!  " + model.getProcessName() + "with advertisement=" + model.getRampup_advertisement() + " with provider=" + model.getProvider() + " has been completed please check ");

					Utility.sendSms("ALERT!!  " + model.getProcessName() + "with advertisement=" + model.getRampup_advertisement() + " with provider=" + model.getProvider() + " has been completed please check ");
				}
				// here we are checking if count is less than the number of
				// rampup count then we insert that count in import table
				else if (rs != null && rs.next()) {

					int count = Integer.parseInt(rs.getString(rs.getString("rowcount")));

					if (count < number_of_users_count_to_insert) {

						number_of_users_count_to_insert = count;
						Utility.sendEmailJustForAlerts("Count is less in table " + model.getFrom_table_name() + " as compare to its rampup count for processName " + model.getProcessName() + " with advertisement= " + model.getRampup_advertisement() + " and provider= " + model.getProvider() + " Please refer!!");
						Utility.sendSms("Count is less in table " + model.getFrom_table_name() + " as compare to its rampup count for processName " + model.getProcessName() + " with advertisement= " + model.getRampup_advertisement() + " and provider= " + model.getProvider() + " Please refer!!");
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// performing rampup only on week days
			if (!Utility.getTheCurrentDayWeek().contains("0") && !Utility.getTheCurrentDayWeek().contains("6")) {

				if (shouldRampUp_Perform) {

					System.out.println(query);

					int query_result = st.executeUpdate(query);

					if (query_result != 0) {
						System.out.println(import_users_rampup_backup_query);
						System.out.println("users has been successfully inserted");
						query_result = st.executeUpdate(import_users_rampup_backup_query);

						if (query_result != 0) {
							String provider = model.getProcessName().split("-")[0];
							provider = model.getProcessName() + "_" + model.getRampup_advertisement();

							String tbl_import_user_count_query = "insert into tbl_import_user_count (date,count,provider,total_count) values (NOW(),'" + number_of_users_count_to_insert + "','" + provider + "','" + number_of_users_count_to_insert + "')";

							if (model.isIs_new_import()) {
								int import_query_result = st.executeUpdate(tbl_import_user_count_query);
							}

							delete_query = "delete from " + model.getFrom_table_name() + whereCondition + " limit " + number_of_users_count_to_insert;

							System.out.println(delete_query);
							boolean is_deleted = false;
							// here we are checking whether to make a copy or
							// move
							if (model.getIs_move_or_copy().equalsIgnoreCase("m")) {
								System.out.println("Users going to move ");
								is_deleted = st.execute(delete_query);
							}

							Utility.sendEmailJustForAlerts("Rampup of " + model.getProcessName() + " with ADVERTISEMENT=" + model.getRampup_advertisement() + "and provider=" + model.getProvider() + " has been performed with users import count=" + model.getRamp_users_count());

						}
					}
				}
			} else {
				// here we keep the ramp-up count same as the last one on week
				// ends
				model.setRamp_users_count(String.valueOf(oldRamp_upcount));
			}

			String today_status = "TRUE";

			// now we are updating them in the rampup table
			String update_lastRecord_query = "update tbl_rampup_process set today_rampup_status='" + today_status + "'  where processName='" + model.getProcessName() + "' and rampup_advertisement='" + model.getRampup_advertisement() + "' and date(date)=date(now())";

			st = Utility.con.createStatement();

			System.out.println(update_lastRecord_query);

			int isUpdated = st.executeUpdate(update_lastRecord_query);
			// upating the record
			if (isUpdated != 0) {
				String insert_new_Record_intable = "INSERT INTO tbl_rampup_process (processName, from_table_name, to_table_name, ramp_users_count, ramp_up_day_count, ramp_up_type, rampup_advertisement, is_advertise_use, is_rampup_live,date" + ",type_of_users,is_move_or_copy,starting_index,percent,is_priority_use,rampup_priority,isDatecondition_use,dayCount,user_domain,is_new_import,provider,domain_type" + ")" + "VALUES (" + "'" + model.getProcessName() + "','" + model.getFrom_table_name() + "','" + model.getTo_table_name() + "','" + model.getRamp_users_count() + "','" + model.getRamp_up_day_count() + "','" + model.getRamp_up_type() + "','" + model.getRampup_advertisement() + "','" + model.isIs_advertise_use() + "','" + model.getIs_rampup_live() + "',NOW()+INTERVAL 1 DAY,'"
						+ model.getType_of_users() + "','" + model.getIs_move_or_copy() + "','" + model.getStarting_index() + "','" + model.getPercent() + "'" + ",'" + model.isIs_priority_use() + "','" + model.getRampup_priority() + "','" + model.isDatecondition_use() + "','" + model.getDayCount() + "',\"" + model.getUser_domain() + "\"" + ",'" + model.isIs_new_import() + "','" + model.getProvider() + "','" + model.getDomain_type() + "')";

				System.out.println(insert_new_Record_intable);

				st = Utility.con.createStatement();

				int insert_new_Record_intable_result = st.executeUpdate(insert_new_Record_intable);

				if (insert_new_Record_intable_result != 0) {

					System.out.println("Successfully updated");
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void sendSms(String message) {

		BufferedReader in = null;
		try {

			// if (Main.isTest == true) {
			// phoneNumber = "8860547656";
			// }
			String phoneNumber = "8860547656,8699864636";
			try {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(baseFilePath + "update_server_count/phonenumberlist.txt"));
				String line = bufferedReader.readLine();
				if (line != null)
					phoneNumber = line;
				bufferedReader.close();
			} catch (Exception e) {
			}
			message = URLEncoder.encode(message, "UTF-8");
			String kanav_number = "";
			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";
			url = url + phoneNumber;
			String text = "&text=" + message;
			url = url + text;
			url = url + "&priority=ndnd&stype=normal";
			System.out.println("Url=" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");
				System.out.println("\nSending 'GET' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);

				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
			}
		} catch (Exception e) {
			try {
				in.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

	public static String initialize_domain_wise_statst_map(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			JSONObject jsonString = new JSONObject(out.toString());
			Utility.preExistingHashMap_DomainWiseHashMap = new Gson().fromJson(jsonString.toString(), new TypeToken<HashMap<String, DomainWiseUserStats>>() {
			}.getType());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("File not found named by " + path);

		}

		// from input stream

		return out.toString();
	}

	public static String initialize_process_wise_statst_map(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			JSONObject jsonString = new JSONObject(out.toString());
			Utility.preExistingHashMap_ProcessWise_HashMap = new Gson().fromJson(jsonString.toString(), new TypeToken<HashMap<String, ProcessWiseUserModel>>() {
			}.getType());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("File not found named by " + path);
		}

		// from input stream

		return out.toString();
	}

	public static void delete_old_file_from_server(String path) {

		System.out.println("File deleting from " + path);
		File file = new File(path);

		try {
			if (file.delete()) {
				System.out.println(file.getName() + " is deleted!");
			} else {
				System.out.println("Delete operation is failed.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Getting exception while delteing the file");

		}
	}

	public static synchronized void write_ivalid_domain_wise_users_inprocess(QueueModelObject model) {
		File log_dashboard_folder = null, dataFile = null;
		FileWriter fileWriterObj = null;

		try {
			log_dashboard_folder = new File(Utility.filePath + "/mis_logs/InvalidDomainWiseUsers/");
			if (!log_dashboard_folder.exists()) {
				log_dashboard_folder.mkdir();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			// this is for oakjob and jobvitals

			dataFile = new File(log_dashboard_folder + File.separator + "invalid_domain_wise_user_" + Utility.dateFormat.format(new Date()) + ".json");
			if (!dataFile.exists()) {
				dataFile.createNewFile();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String jasonData = new Gson().toJson(Utility.invalidDomain_users_count_map);

		try {
			// writitng the stats in the file
			String queueData = "";
			fileWriterObj = new FileWriter(dataFile);
			try {
				fileWriterObj.write(jasonData);
				fileWriterObj.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {

		}

	}

	public static String initialize_invalid_domain_wise_statst_map(String path) {

		InputStream in;
		StringBuilder out = new StringBuilder();
		try {
			in = new FileInputStream(new File(path));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			String line;
			while ((line = reader.readLine()) != null) {
				out.append(line);
			}
			JSONObject jsonString = new JSONObject(out.toString());
			Utility.invalidDomain_users_count_map = new Gson().fromJson(jsonString.toString(), new TypeToken<HashMap<String, InvalidDomainModel>>() {
			}.getType());

			reader.close();
			// Prints the string content
			// read
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("File not found named by " + path);
		}

		// from input stream

		return out.toString();
	}

	public static void updateRampupObjectInTable(QueueModelObject model) {

		try {
			if (Utility.con == null || Utility.con.isClosed()) {
				Utility.con = openConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int lastCount = Integer.parseInt(model.getUpTo());
		System.out.println("lastcount=" + lastCount);
		if (!model.getRamup_type().equalsIgnoreCase("fixed")) {

			lastCount = (lastCount * 2) + lastCount * 25 / 100;
		}

		String query = "update user_process_lifecycle_dummy set upto='" + lastCount + "',today_rampup_status=date(now()+interval 1 day) where id='" + model.getId() + "'";

		try {
			Statement st = con.createStatement();

			int isUpdated = st.executeUpdate(query);
			System.out.println("query for update=" + query);
			// upating the record
			if (isUpdated != 0) {
				model.setUpTo("" + lastCount);
				System.out.println("successfully updated");
				Utility.sendEmailJustForAlerts("RAMPUP OF " + model.getProcessName() + " with prcesstype= " + model.getProcessType() + "has been performed with count " + lastCount);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HashMap<String, String> channelWiseHashMap = new HashMap<String, String>();
	public static String channelNotFound = "";

	public static void initializeChannelWithProvider() {

		String query = "select job_seeker,channel from tbl_seekers_wise_data";

		try {
			if (Utility.con == null || Utility.con.isClosed()) {
				Utility.con = openConnection();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			Statement st = con.createStatement();
			ResultSet res = st.executeQuery(query);

			while (!res.isClosed() && res.next()) {

				Utility.channelWiseHashMap.put(res.getString("job_seeker".trim().toLowerCase()), res.getString("channel").trim());

			}

			System.out.println("SIZE OF CHANNELWISE HASHMAP=" + Utility.channelWiseHashMap.size());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static boolean isTodayDate(String rampup_date) {
		Date date;
		try {
			date = Utility.dateFormat.parse(rampup_date);
			System.out.println(date);
			rampup_date = Utility.dateFormat.format(date);
			System.out.println(rampup_date);

			System.out.println();

			String CurrentDate = Utility.dateFormat.format(new Date());

			if (rampup_date.equalsIgnoreCase(CurrentDate)) {
				return true;

			}

		} catch (ParseException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		return false;
	}

	public static void writeDataInCsv_for_whitelable_with_domain_advertisement(HashMap<String, DomainWiseSeekersCountModel> map) {
		String outputFile = Utility.baseFilePath + "queuemaking/whitelable_domain_jobseekers.csv";

		// before we open the file check to see if it already exists
		boolean alreadyExists = new File(outputFile).exists();

		try {
			// use FileWriter constructor that specifies open for appending
			CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile), ',');

			// if the file didn't already exist then we need to write out the
			// header line
			if (!alreadyExists) {
				csvOutput.write("Whitelable");
				csvOutput.write("Domain");
				csvOutput.write("EmailClient");
				csvOutput.write("Seekers Wise Count");
				csvOutput.write("Total Count");
				csvOutput.endRecord();
			}
			// else assume that the file already has the correct header line

			for (Map.Entry<String, DomainWiseSeekersCountModel> entry : map.entrySet()) {
				System.out.println(entry.getKey() + "/" + entry.getValue());
				DomainWiseSeekersCountModel model = entry.getValue();

				csvOutput.write(model.getWhitelabelName());

				for (String domainName : model.getDomainsList()) {
					csvOutput.write(domainName);
					csvOutput.write(model.getDomain_email_client().get(domainName));
					HashMap<String, Integer> internal_advertisement_map = model.getMap_with_count().get(domainName);
					String domain_advertisement_and_its_count_string = "";
					Integer total_advertisement_count = 0;
					for (Map.Entry<String, Integer> advertisement_map_entry : internal_advertisement_map.entrySet()) {

						// here we writng the advertimsent wise data
						domain_advertisement_and_its_count_string = domain_advertisement_and_its_count_string + "" + advertisement_map_entry.getKey() + " - " + advertisement_map_entry.getValue() + "\n";
						total_advertisement_count = total_advertisement_count + advertisement_map_entry.getValue();
					}
					csvOutput.write(domain_advertisement_and_its_count_string);
					csvOutput.write("" + total_advertisement_count);

				}
				csvOutput.endRecord();
			}
			// write out a few records

			csvOutput.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
